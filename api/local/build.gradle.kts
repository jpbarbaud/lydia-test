plugins {
    plugins("jvm-library")
    kotlin("plugin.serialization") version Config.kotlinVersion
}

dependencies {
    implementation(Dependencies.Serialization.json)
}
package fr.jpbarbaud.api.local

interface LocalUserStore {
    suspend fun clearAll()

    suspend fun save(list: List<LocalUser>)

    suspend fun getAll(): List<LocalUser>
}
package fr.jpbarbaud.api.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiUser(
    @Json(name = "gender")
    val gender: Gender,
    @Json(name = "name")
    val name: Name,
    @Json(name = "location")
    val location: Location,
    @Json(name = "email")
    val email: String,
    @Json(name = "login")
    val login: Login,
    @Json(name = "registered")
    val registered: Long,
    @Json(name = "dob")
    val dob: Long,
    @Json(name = "phone")
    val phone: String,
    @Json(name = "cell")
    val cell: String,
    @Json(name = "id")
    val id: Id,
    @Json(name = "picture")
    val picture: Picture,
    @Json(name = "nat")
    val nat: Nat,
)

enum class Gender {
    @Json(name = "male")
    MALE,
    @Json(name = "female")
    FEMALE
}

@JsonClass(generateAdapter = true)
data class Name(
    @Json(name = "title")
    val title: String,
    @Json(name = "first")
    val first: String,
    @Json(name = "last")
    val last: String,
)

@JsonClass(generateAdapter = true)
data class Location(
    @Json(name = "street")
    val street: String,
    @Json(name = "city")
    val city: String,
    @Json(name = "state")
    val state: String,
    @Json(name = "postcode")
    val postcode: String,
)

@JsonClass(generateAdapter = true)
data class Login(
    @Json(name = "username")
    val username: String,
    @Json(name = "password")
    val password: String,
    @Json(name = "salt")
    val salt: String,
    @Json(name = "md5")
    val md5: String,
    @Json(name = "sha1")
    val sha1: String,
    @Json(name = "sha256")
    val sha256: String,
)

@JsonClass(generateAdapter = true)
data class Id(
    @Json(name = "name")
    val name: String,
    @Json(name = "value")
    val value: String?,
)

@JsonClass(generateAdapter = true)
data class Picture(
    @Json(name = "large")
    val large: String,
    @Json(name = "medium")
    val medium: String,
    @Json(name = "thumbnail")
    val thumbnail: String,
)

enum class Nat {
    @Json(name = "AU") AU,
    @Json(name = "BR") BR,
    @Json(name = "CA") CA,
    @Json(name = "CH") CH,
    @Json(name = "DE") DE,
    @Json(name = "DK") DK,
    @Json(name = "ES") ES,
    @Json(name = "FI") FI,
    @Json(name = "FR") FR,
    @Json(name = "GB") GB,
    @Json(name = "IE") IE,
    @Json(name = "IR") IR,
    @Json(name = "NL") NL,
    @Json(name = "NZ") NZ,
    @Json(name = "TR") TR,
    @Json(name = "US") US
}
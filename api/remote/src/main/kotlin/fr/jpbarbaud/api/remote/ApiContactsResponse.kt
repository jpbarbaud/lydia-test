package fr.jpbarbaud.api.remote

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiContactsResponse(
    @Json(name = "results")
    val results: List<ApiUser>,
)
package fr.jpbarbaud.api.remote

import retrofit2.http.GET
import retrofit2.http.Query

interface ContactsApiService {
    @GET("api/1.0/?seed=lydia")
    suspend fun fetchContacts(@Query("page") page: Int, @Query("results") resultsPerPage: Int) : ApiContactsResponse
}
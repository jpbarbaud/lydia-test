package fr.jpbarbaud.api.remote.di

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

private const val READ_TIMEOUT_IN_SEC = 30L
private const val WRITE_TIMEOUT_IN_SEC = 30L
private const val CONNECTION_TIMEOUT_IN_SEC = 30L

private const val BASE_URL = "https://randomuser.me/"

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideOkHttpLoggingInterceptor(): Interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Provides
    @Reusable
    fun provideOkHttpClient(interceptor: Interceptor): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(CONNECTION_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
        .writeTimeout(WRITE_TIMEOUT_IN_SEC, TimeUnit.SECONDS)
        .addInterceptor(interceptor)
        .build()

    @Provides
    @Reusable
    internal fun provideMoshi(): Moshi = Moshi.Builder().build()

    @Provides
    @Reusable
    fun provideRetrofit(client: OkHttpClient, moshi: Moshi) : Retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL)
        .client(client)
        .build()
}
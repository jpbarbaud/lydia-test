plugins {
    plugins("jvm-library")
    kotlin("kapt")
}

dependencies {
    implementations(
        Dependencies.Hilt.hiltCore,
        Dependencies.Http.moshi,
        Dependencies.Http.moshiConverter,
        Dependencies.Http.okhttpLoggingInterceptor,
        Dependencies.Http.retrofit,
    )
    kapt("com.google.dagger:hilt-compiler:2.41")
    kapt("com.squareup.moshi:moshi-kotlin-codegen:1.13.0")
}
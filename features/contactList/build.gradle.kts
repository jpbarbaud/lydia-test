plugins {
    plugins(
        "android-library",
        "dagger.hilt.android.plugin"
    )
    kotlin("kapt")
}

dependencies {
    implementation(project(":libraries:commonAndroid"))
    implementation(project(":libraries:navigationAndroid"))
    implementation(project(":presentation"))
    implementation(project(":repositories"))
    implementations(
        Dependencies.glide,
        Dependencies.Hilt.hiltAndroid,
        Dependencies.AndroidX.recyclerView,
        Dependencies.AndroidX.swipeRefreshLayout,
    )
    kapt("com.google.dagger:hilt-compiler:2.41")
}
package fr.jpbarbaud.lydiapp.features.contactlist

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import fr.jpbarbaud.lydiapp.features.contactlist.adapter.ContactListAdapter
import fr.jpbarbaud.lydiapp.features.contactlist.adapter.ContactListLoadStateAdapter
import fr.jpbarbaud.lydiapp.features.contactlist.databinding.FragmentContactListBinding
import fr.jpbarbaud.lydiapp.libraries.navigationandroid.ContactDetailActivityNavigator
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class ContactListFragment : Fragment(R.layout.fragment_contact_list) {

    private var _binding: FragmentContactListBinding? = null
    private val binding: FragmentContactListBinding get() = requireNotNull(_binding)

    private val viewModel: ContactListViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = FragmentContactListBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            val layoutManager =
                requireNotNull((contactListRecycler.layoutManager as LinearLayoutManager)) {
                    "LayoutManager must be set"
                }

            contactListRecycler.addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    layoutManager.orientation
                )
            )

            swipeContainer.setOnRefreshListener {
                viewModel.onRefresh()
            }
        }

        initAdapter()

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.navigationDetailEvent.collectLatest { parcelableUser ->
                startActivity(
                    ContactDetailActivityNavigator.newIntent(requireContext(), parcelableUser)
                )
            }
        }
    }

    private fun initAdapter() = with(binding) {
        val contactListAdapter = ContactListAdapter { position ->
            viewModel.onContactClicked(position)
        }

        subscribeToListAdapterObservable(contactListAdapter)

        val contactListLoadStateAdapter = ContactListLoadStateAdapter({
            viewModel.onLoadMoreClicked()
        }, {
            viewModel.onRetry()
        })

        subscribeToLoadStateAdapterObservable(contactListLoadStateAdapter)

        contactListRecycler.adapter = ConcatAdapter(
            contactListAdapter,
            contactListLoadStateAdapter
        )
    }

    private fun subscribeToListAdapterObservable(adapter: ContactListAdapter) = with(binding) {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.contactListPresentationList.collect {
                if (swipeContainer.isRefreshing && it.isNotEmpty()) {
                    swipeContainer.isRefreshing = false
                }
                adapter.updateLists(it)
            }
        }
    }

    private fun subscribeToLoadStateAdapterObservable(adapter: ContactListLoadStateAdapter) =
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.contactListState.collectLatest {
                adapter.loadState = it
            }
        }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
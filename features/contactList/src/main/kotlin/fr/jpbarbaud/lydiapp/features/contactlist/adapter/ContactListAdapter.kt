package fr.jpbarbaud.lydiapp.features.contactlist.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import fr.jpbarbaud.lydiapp.presentation.model.ContactListPresentationItem

class ContactListAdapter constructor(private val onItemClick: (itemPosition: Int) -> Unit) :
    RecyclerView.Adapter<ContactListViewHolder>() {

    private var contactList: List<ContactListPresentationItem> = emptyList()

    fun updateLists(newItems: List<ContactListPresentationItem>) {
        val contactListDiffUtilCallback =
            ContactListDiffUtilCallback(newItems = newItems, oldItems = contactList)
        contactList = newItems
        DiffUtil.calculateDiff(contactListDiffUtilCallback).dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactListViewHolder =
        ContactListViewHolder(parent).apply {
            itemView.setOnClickListener {
                onItemClick.invoke(bindingAdapterPosition)
            }
        }

    override fun onBindViewHolder(holder: ContactListViewHolder, position: Int) {
        holder.bind(contactList[position])
    }

    override fun getItemCount(): Int = contactList.size

    override fun onViewRecycled(holder: ContactListViewHolder) {
        holder.recycling()
        super.onViewRecycled(holder)
    }

    private class ContactListDiffUtilCallback constructor(
        private val newItems: List<ContactListPresentationItem>,
        private val oldItems: List<ContactListPresentationItem>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldItems[oldItemPosition].email == newItems[newItemPosition].email

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldItems[oldItemPosition] == newItems[newItemPosition]
    }

}
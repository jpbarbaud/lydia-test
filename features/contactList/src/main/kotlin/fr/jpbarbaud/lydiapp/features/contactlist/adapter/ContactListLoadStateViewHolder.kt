package fr.jpbarbaud.lydiapp.features.contactlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import fr.jpbarbaud.lydiapp.features.contactlist.R
import fr.jpbarbaud.lydiapp.features.contactlist.databinding.ContactListLoadStateRowBinding
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.ContactListPresentationModelState

class ContactListLoadStateViewHolder private constructor(
    private val binding: ContactListLoadStateRowBinding,
    loadMore: () -> Unit,
    retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    constructor(parent: ViewGroup, loadMore: () -> Unit, retry: () -> Unit) : this(
        binding = ContactListLoadStateRowBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ),
        loadMore = loadMore,
        retry = retry
    )

    init {
        binding.retryButton.also {
            it.setOnClickListener { retry.invoke() }
        }
        binding.loadMoreButton.also {
            it.setOnClickListener { loadMore.invoke() }
        }
    }

    fun bind(loadState: ContactListPresentationModelState) = with(binding) {
        if (loadState is ContactListPresentationModelState.Error) {
            // Todo Show more accurate error message
            errorMessage.text =
                itemView.context.getString(R.string.constact_list_item_generic_error_message)
        }

        loadMoreButton.isVisible = loadState is ContactListPresentationModelState.Success
        progressBar.isVisible = loadState == ContactListPresentationModelState.Loading
        retryButton.isVisible = loadState == ContactListPresentationModelState.Error
        errorMessage.isVisible = loadState == ContactListPresentationModelState.Error
    }
}
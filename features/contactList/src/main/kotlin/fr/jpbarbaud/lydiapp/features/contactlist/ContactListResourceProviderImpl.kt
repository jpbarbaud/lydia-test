package fr.jpbarbaud.lydiapp.features.contactlist

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.ContactListResourceProvider
import javax.inject.Inject

class ContactListResourceProviderImpl @Inject constructor(
    @ApplicationContext val context: Context
) : ContactListResourceProvider {
    override val emptyMessage: String get() = context.getString(R.string.contact_list_empty_message)
}
package fr.jpbarbaud.lydiapp.features.contactlist

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.jpbarbaud.lydiapp.features.contactlist.mapper.toParcelableUserList
import fr.jpbarbaud.lydiapp.libraries.commonandroid.domain.model.ParcelableUser
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.ContactListPresentationModel
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.ContactListPresentationModelState
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.ContactListResourceProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
@HiltViewModel
class ContactListViewModel @Inject constructor(
    private val presentationModel: ContactListPresentationModel,
    private val contactListResourceProvider: ContactListResourceProvider
) : ViewModel() {

    private val _navigationDetailEvent = MutableSharedFlow<ParcelableUser>()
    val navigationDetailEvent: SharedFlow<ParcelableUser> = _navigationDetailEvent.asSharedFlow()

    private val _contactListState =
        MutableStateFlow<ContactListPresentationModelState>(ContactListPresentationModelState.Loading)
    val contactListState: StateFlow<ContactListPresentationModelState> =
        _contactListState.asStateFlow()

    val contactListPresentationList = presentationModel.contactListPresentationResultFlow
        .onEach {
            _contactListState.value = if (it.isEmpty()) {
                ContactListPresentationModelState.Empty(contactListResourceProvider.emptyMessage)
            } else {
                ContactListPresentationModelState.Success(it)
            }
        }.shareIn(viewModelScope, started = SharingStarted.WhileSubscribed(), replay = 1)

    private val parcelableUserList = presentationModel.detailPresentationResultFlow
        .mapLatest { it.toParcelableUserList() }
        .shareIn(scope = viewModelScope, started = SharingStarted.Eagerly, replay = 1)

    init {
        _contactListState.value = ContactListPresentationModelState.Loading
        viewModelScope.launch {
            presentationModel.start()
                .onFailure(::onFailure)
        }
    }

    fun onRefresh() {
        viewModelScope.launch {
            presentationModel.refresh()
        }
    }

    fun onRetry() {
        onLoadMoreClicked()
    }

    fun onLoadMoreClicked() {
        _contactListState.value = ContactListPresentationModelState.Loading
        viewModelScope.launch {
            presentationModel.fetchNext()
                .onFailure(::onFailure)
        }
    }

    fun onContactClicked(position: Int) {
        viewModelScope.launch {
            val parcelableUserList = parcelableUserList.first()
            _navigationDetailEvent.emit(parcelableUserList[position])
        }
    }

    private fun onFailure(throwable: Throwable) {
        Log.e("ContactListViewModel", throwable.toString())
        _contactListState.value = ContactListPresentationModelState.Error
    }
}
package fr.jpbarbaud.lydiapp.features.contactlist.mapper

import fr.jpbarbaud.lydiapp.libraries.commonandroid.domain.model.ParcelableUser
import fr.jpbarbaud.lydiapp.presentation.model.DetailPresentationItem

fun List<DetailPresentationItem>.toParcelableUserList(): List<ParcelableUser> =
    map { it.toParcelableUser() }

fun DetailPresentationItem.toParcelableUser(): ParcelableUser = ParcelableUser(
    gender = gender.toParcelable(),
    name = name.toParcelable(),
    location = location.toParcelable(),
    email = email,
    login = login.toParcelable(),
    registered = registered,
    dob = dob,
    phone = phone,
    cell = cell,
    id = id.toParcelable(),
    picture = picture.toParcelable(),
    nat = nat.toParcelable()
)

private fun DetailPresentationItem.Gender.toParcelable(): ParcelableUser.Gender = when (this) {
    DetailPresentationItem.Gender.MALE -> ParcelableUser.Gender.MALE
    DetailPresentationItem.Gender.FEMALE -> ParcelableUser.Gender.FEMALE
}

private fun DetailPresentationItem.Name.toParcelable(): ParcelableUser.Name =
    ParcelableUser.Name(
        title = title,
        first = first,
        last = last
    )

private fun DetailPresentationItem.Location.toParcelable(): ParcelableUser.Location =
    ParcelableUser.Location(
        street = street,
        city = city,
        state = state,
        postcode = postcode
    )

private fun DetailPresentationItem.Login.toParcelable(): ParcelableUser.Login =
    ParcelableUser.Login(
        username = username,
        password = password,
        salt = salt,
        md5 = md5,
        sha1 = sha1,
        sha256 = sha256
    )

private fun DetailPresentationItem.Id.toParcelable(): ParcelableUser.Id = ParcelableUser.Id(
    name = name,
    value = value
)

private fun DetailPresentationItem.Picture.toParcelable(): ParcelableUser.Picture =
    ParcelableUser.Picture(
        large = large,
        medium = medium,
        thumbnail = thumbnail
    )

private fun DetailPresentationItem.Nat.toParcelable(): ParcelableUser.Nat = when (this) {
    DetailPresentationItem.Nat.AU -> ParcelableUser.Nat.AU
    DetailPresentationItem.Nat.BR -> ParcelableUser.Nat.BR
    DetailPresentationItem.Nat.CA -> ParcelableUser.Nat.CA
    DetailPresentationItem.Nat.CH -> ParcelableUser.Nat.CH
    DetailPresentationItem.Nat.DE -> ParcelableUser.Nat.DE
    DetailPresentationItem.Nat.DK -> ParcelableUser.Nat.DK
    DetailPresentationItem.Nat.ES -> ParcelableUser.Nat.ES
    DetailPresentationItem.Nat.FI -> ParcelableUser.Nat.FI
    DetailPresentationItem.Nat.FR -> ParcelableUser.Nat.FR
    DetailPresentationItem.Nat.GB -> ParcelableUser.Nat.GB
    DetailPresentationItem.Nat.IE -> ParcelableUser.Nat.IE
    DetailPresentationItem.Nat.IR -> ParcelableUser.Nat.IR
    DetailPresentationItem.Nat.NL -> ParcelableUser.Nat.NL
    DetailPresentationItem.Nat.NZ -> ParcelableUser.Nat.NZ
    DetailPresentationItem.Nat.TR -> ParcelableUser.Nat.TR
    DetailPresentationItem.Nat.US -> ParcelableUser.Nat.US
}
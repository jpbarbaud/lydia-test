package fr.jpbarbaud.lydiapp.features.contactlist.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.ContactListPresentationModelState

class ContactListLoadStateAdapter(
    private val loadMore: () -> Unit,
    private val retry: () -> Unit
) : RecyclerView.Adapter<ContactListLoadStateViewHolder>() {
    /**
     * LoadState to present in the adapter.
     *
     * Changing this property will immediately notify the Adapter to change the item it's
     * presenting.
     */
    var loadState: ContactListPresentationModelState = ContactListPresentationModelState.Done
        set(loadState) {
            if (field != loadState) {
                val displayOldItem = displayLoadStateAsItem(field)
                val displayNewItem = displayLoadStateAsItem(loadState)

                if (displayOldItem && !displayNewItem) {
                    notifyItemRemoved(0)
                } else if (displayNewItem && !displayOldItem) {
                    notifyItemInserted(0)
                } else if (displayOldItem && displayNewItem) {
                    notifyItemChanged(0)
                }
                field = loadState
            }
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ContactListLoadStateViewHolder = ContactListLoadStateViewHolder(
        parent = parent,
        loadMore = loadMore,
        retry = retry
    )

    override fun onBindViewHolder(holder: ContactListLoadStateViewHolder, position: Int) =
        holder.bind(loadState)

    override fun getItemViewType(position: Int): Int = 0

    override fun getItemCount(): Int = if (displayLoadStateAsItem(loadState)) 1 else 0

    private fun displayLoadStateAsItem(loadState: ContactListPresentationModelState): Boolean =
        loadState is ContactListPresentationModelState.Loading
                || loadState is ContactListPresentationModelState.Error
                || loadState is ContactListPresentationModelState.Success
}
package fr.jpbarbaud.lydiapp.features.contactlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import fr.jpbarbaud.lydiapp.features.contactlist.R
import fr.jpbarbaud.lydiapp.features.contactlist.databinding.ContactListRowBinding
import fr.jpbarbaud.lydiapp.libraries.commonandroid.ui.clearLoadings
import fr.jpbarbaud.lydiapp.libraries.commonandroid.ui.load
import fr.jpbarbaud.lydiapp.presentation.model.ContactListPresentationItem

class ContactListViewHolder private constructor(
    private val binding: ContactListRowBinding,
) : RecyclerView.ViewHolder(binding.root) {

    constructor(parent: ViewGroup) : this(
        binding = ContactListRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    fun bind(model: ContactListPresentationItem) = with(binding) {
        contactThumbnail.load(model.thumbnail)
        contactFullName.text = itemView.context.getString(
            R.string.constact_list_item_full_name,
            model.firstName,
            model.lastName
        )
        contactEmail.text = model.email
    }

    fun recycling() {
        binding.contactThumbnail.clearLoadings()
    }
}
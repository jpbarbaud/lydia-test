package fr.jpbarbaud.lydiapp.features.contactlist.di

import dagger.Binds
import dagger.Module
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.jpbarbaud.lydiapp.features.contactlist.ContactListResourceProviderImpl
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.ContactListResourceProvider

@Module
@InstallIn(SingletonComponent::class)
abstract class ContactListModule {

    @Binds
    @Reusable
    abstract fun provideContactListResourceProvider(provider: ContactListResourceProviderImpl): ContactListResourceProvider
}
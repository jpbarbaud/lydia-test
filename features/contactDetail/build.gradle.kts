plugins {
    plugins(
        "android-library"
    )
    kotlin("kapt")
}

dependencies {
    implementation(project(":libraries:commonAndroid"))
    implementation(project(":libraries:navigationAndroid"))
    implementation(project(":presentation"))
    implementation(project(":repositories"))
    implementations(
        Dependencies.glide
    )
}
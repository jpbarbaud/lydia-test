package fr.jpbarbaud.lydiapp.features.contactdetail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import fr.jpbarbaud.lydiapp.features.contactdetail.databinding.ActivityContactDetailBinding
import fr.jpbarbaud.lydiapp.libraries.commonandroid.domain.model.ParcelableUser
import fr.jpbarbaud.lydiapp.libraries.navigationandroid.ContactDetailActivityNavigator

class ContactDetailActivity : AppCompatActivity() {
    private var _binding: ActivityContactDetailBinding? = null
    private val binding: ActivityContactDetailBinding get() = requireNotNull(_binding)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityContactDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val parcelableUser =
            requireNotNull(intent.getParcelableExtra<ParcelableUser>(ContactDetailActivityNavigator.CONTACT_DETAIL_ACTIVITY_PRESENTATION_ID_KEY)) {
                "parcelableUser should not be null"
            }

        val detailFragment = ContactDetailFragment.newInstance(parcelableUser)
        supportFragmentManager.commit {
            add(binding.container.id, detailFragment, ContactDetailFragment.TAG)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
package fr.jpbarbaud.lydiapp.features.contactdetail.mapper

import fr.jpbarbaud.lydiapp.libraries.commonandroid.domain.model.ParcelableUser
import fr.jpbarbaud.lydiapp.presentation.model.DetailPresentationItem

fun ParcelableUser.toDetailPresentationItem(): DetailPresentationItem = DetailPresentationItem(
    gender = gender.toDetailPresentation(),
    name = name.toDetailPresentation(),
    location = location.toDetailPresentation(),
    email = email,
    login = login.toDetailPresentation(),
    registered = registered,
    dob = dob,
    phone = phone,
    cell = cell,
    id = id.toDetailPresentation(),
    picture = picture.toDetailPresentation(),
    nat = nat.toDetailPresentation()
)

private fun ParcelableUser.Gender.toDetailPresentation(): DetailPresentationItem.Gender =
    when (this) {
        ParcelableUser.Gender.MALE -> DetailPresentationItem.Gender.MALE
        ParcelableUser.Gender.FEMALE -> DetailPresentationItem.Gender.FEMALE
    }

private fun ParcelableUser.Name.toDetailPresentation(): DetailPresentationItem.Name =
    DetailPresentationItem.Name(
        title = title,
        first = first,
        last = last
    )

private fun ParcelableUser.Location.toDetailPresentation(): DetailPresentationItem.Location =
    DetailPresentationItem.Location(
        street = street,
        city = city,
        state = state,
        postcode = postcode
    )

private fun ParcelableUser.Login.toDetailPresentation(): DetailPresentationItem.Login =
    DetailPresentationItem.Login(
        username = username,
        password = password,
        salt = salt,
        md5 = md5,
        sha1 = sha1,
        sha256 = sha256
    )

private fun ParcelableUser.Id.toDetailPresentation(): DetailPresentationItem.Id =
    DetailPresentationItem.Id(
        name = name,
        value = value
    )

private fun ParcelableUser.Picture.toDetailPresentation(): DetailPresentationItem.Picture =
    DetailPresentationItem.Picture(
        large = large,
        medium = medium,
        thumbnail = thumbnail
    )

private fun ParcelableUser.Nat.toDetailPresentation(): DetailPresentationItem.Nat = when (this) {
    ParcelableUser.Nat.AU -> DetailPresentationItem.Nat.AU
    ParcelableUser.Nat.BR -> DetailPresentationItem.Nat.BR
    ParcelableUser.Nat.CA -> DetailPresentationItem.Nat.CA
    ParcelableUser.Nat.CH -> DetailPresentationItem.Nat.CH
    ParcelableUser.Nat.DE -> DetailPresentationItem.Nat.DE
    ParcelableUser.Nat.DK -> DetailPresentationItem.Nat.DK
    ParcelableUser.Nat.ES -> DetailPresentationItem.Nat.ES
    ParcelableUser.Nat.FI -> DetailPresentationItem.Nat.FI
    ParcelableUser.Nat.FR -> DetailPresentationItem.Nat.FR
    ParcelableUser.Nat.GB -> DetailPresentationItem.Nat.GB
    ParcelableUser.Nat.IE -> DetailPresentationItem.Nat.IE
    ParcelableUser.Nat.IR -> DetailPresentationItem.Nat.IR
    ParcelableUser.Nat.NL -> DetailPresentationItem.Nat.NL
    ParcelableUser.Nat.NZ -> DetailPresentationItem.Nat.NZ
    ParcelableUser.Nat.TR -> DetailPresentationItem.Nat.TR
    ParcelableUser.Nat.US -> DetailPresentationItem.Nat.US
}
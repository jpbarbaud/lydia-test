package fr.jpbarbaud.lydiapp.features.contactdetail

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import fr.jpbarbaud.lydiapp.features.contactdetail.databinding.FragmentContactDetailBinding
import fr.jpbarbaud.lydiapp.features.contactdetail.mapper.toDetailPresentationItem
import fr.jpbarbaud.lydiapp.libraries.commonandroid.domain.model.ParcelableUser
import fr.jpbarbaud.lydiapp.libraries.commonandroid.ui.load
import fr.jpbarbaud.lydiapp.presentation.model.DetailPresentationItem

class ContactDetailFragment : Fragment(R.layout.fragment_contact_detail) {

    private var _binding: FragmentContactDetailBinding? = null
    private val binding: FragmentContactDetailBinding get() = requireNotNull(_binding)

    companion object {
        internal fun newInstance(parcelableUser: ParcelableUser) = ContactDetailFragment().apply {
            arguments = bundleOf(
                PARCELABLE_USER_ARG_KEY to parcelableUser
            )
        }

        const val TAG = "ContactDetailFragment"
        private const val PARCELABLE_USER_ARG_KEY = "parcelable_user"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = FragmentContactDetailBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)

        val presentationItem =
            requireNotNull(requireArguments().getParcelable<ParcelableUser>(PARCELABLE_USER_ARG_KEY)) {
                "parcelableUser should not be null"
            }.toDetailPresentationItem()

        initToolbar(presentationItem.name)
        bindData(presentationItem)
    }

    private fun initToolbar(name: DetailPresentationItem.Name) {
        with(requireActivity() as AppCompatActivity) {
            setSupportActionBar(binding.toolbar)
            binding.toolbar.setNavigationOnClickListener { onBackPressed() }
            binding.toolbar.title = getString(R.string.toolbar_title, name.first, name.last)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun bindData(presentationItem: DetailPresentationItem) {
        bindPicture(presentationItem.picture)
        bindIdentity(presentationItem.name)
        bindGender(presentationItem.gender)
        bindNationality(presentationItem.nat)
        bindPhones(presentationItem.phone, presentationItem.cell)
        bindEmail(presentationItem.email)
        bindAddress(presentationItem.location)
        bindLogin(presentationItem.login)
        bindOther(presentationItem.registered, presentationItem.dob)
    }

    private fun bindPicture(picture: DetailPresentationItem.Picture) =
        binding.picture.load(picture.large)

    private fun bindIdentity(name: DetailPresentationItem.Name) {
        binding.fullname.text = getString(R.string.full_name, name.title, name.first, name.last)
    }

    private fun bindGender(gender: DetailPresentationItem.Gender) {
        binding.gender.text = when (gender) {
            DetailPresentationItem.Gender.MALE -> getString(R.string.gender_man)
            DetailPresentationItem.Gender.FEMALE -> getString(R.string.gender_woman)
        }
    }

    private fun bindNationality(nat: DetailPresentationItem.Nat) {
        binding.flag.text = when (nat) {
            DetailPresentationItem.Nat.AU -> getString(R.string.nat_au)
            DetailPresentationItem.Nat.BR -> getString(R.string.nat_br)
            DetailPresentationItem.Nat.CA -> getString(R.string.nat_ca)
            DetailPresentationItem.Nat.CH -> getString(R.string.nat_ch)
            DetailPresentationItem.Nat.DE -> getString(R.string.nat_de)
            DetailPresentationItem.Nat.DK -> getString(R.string.nat_dk)
            DetailPresentationItem.Nat.ES -> getString(R.string.nat_es)
            DetailPresentationItem.Nat.FI -> getString(R.string.nat_fi)
            DetailPresentationItem.Nat.FR -> getString(R.string.nat_fr)
            DetailPresentationItem.Nat.GB -> getString(R.string.nat_gb)
            DetailPresentationItem.Nat.IE -> getString(R.string.nat_ie)
            DetailPresentationItem.Nat.IR -> getString(R.string.nat_ir)
            DetailPresentationItem.Nat.NL -> getString(R.string.nat_nl)
            DetailPresentationItem.Nat.NZ -> getString(R.string.nat_nz)
            DetailPresentationItem.Nat.TR -> getString(R.string.nat_tr)
            DetailPresentationItem.Nat.US -> getString(R.string.nat_us)
        }
    }

    private fun bindPhones(phone: String, cell: String) {
        binding.phone.text = phone
        binding.cell.text = cell
    }

    private fun bindEmail(email: String) {
        binding.email.text = email
    }

    private fun bindAddress(location: DetailPresentationItem.Location) {
        binding.address.text = getString(
            R.string.address_text,
            location.street,
            location.postcode,
            location.city,
            location.state
        )
    }

    private fun bindLogin(login: DetailPresentationItem.Login) {
        binding.loginUsername.text = getString(R.string.login_username, login.username)
        binding.loginPassword.text = getString(R.string.login_password, login.password)
        binding.loginSalt.text = getString(R.string.login_salt, login.salt)
        binding.loginMd5.text = getString(R.string.login_md5, login.md5)
        binding.loginSha1.text = getString(R.string.login_sha1, login.sha1)
        binding.loginSha256.text = getString(R.string.login_sha256, login.sha256)
    }

    private fun bindOther(registered: Long, dob: Long) {
        binding.otherRegistered.text = registered.toString()
        binding.otherDob.text = dob.toString()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
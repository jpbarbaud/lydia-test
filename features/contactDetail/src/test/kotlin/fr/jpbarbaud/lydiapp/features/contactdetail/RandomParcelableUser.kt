package fr.jpbarbaud.lydiapp.features.contactdetail

import fr.jpbarbaud.lydiapp.libraries.commonandroid.domain.model.ParcelableUser
import kotlin.random.Random
import kotlin.random.nextUInt

// Todo String are not so random as we wanted

fun Random.nextParcelableUser(): ParcelableUser = ParcelableUser(
    gender = ParcelableUser.Gender.values().random(),
    name = nextUserName(),
    location = nextUserLocation(),
    email = "randomMe@binmail.fr",
    login = nextUserLogin(),
    registered = 1192836162,
    dob = 1030053735,
    phone = "02-2112-2147",
    cell = "0404-433-992",
    id = nextUserId(),
    picture = nextUserPicture(),
    nat = ParcelableUser.Nat.values().random()
)

private fun nextUserName(): ParcelableUser.Name = ParcelableUser.Name(
    title = "M",
    first = "Kevin",
    last = "Smith"
)

private fun Random.nextUserLocation(): ParcelableUser.Location = ParcelableUser.Location(
    street = "2876 oak lawn ave",
    city = "Paris",
    state = "Ile de France",
    postcode = nextUInt().toString()
)

private fun Random.nextUserLogin(): ParcelableUser.Login = ParcelableUser.Login(
    username = "Jipe",
    password = nextDouble().toString(),
    salt = "e5e6c5606704f4655076c7a71ac77be7",
    md5 = "701d3375874edd63fbd5e7b1446be7c2ba8dc6ee",
    sha1 = "701d3375874edd63fbd5e7b1446be7c2ba8dc6ee",
    sha256 = "c17a7f6874370cecebecff65aed8613fd78bbbe0f4a439e32cc1ecf389bb02ab"
)

private fun nextUserId(): ParcelableUser.Id = ParcelableUser.Id(
    name = "TFN",
    value = "3677373"
)

private fun nextUserPicture(): ParcelableUser.Picture = ParcelableUser.Picture(
    large = "https://randomuser.me/api/portraits/women/21.jpg",
    medium = "https://randomuser.me/api/portraits/med/women/21.jpg",
    thumbnail = "https://randomuser.me/api/portraits/thumb/women/21.jpg"
)
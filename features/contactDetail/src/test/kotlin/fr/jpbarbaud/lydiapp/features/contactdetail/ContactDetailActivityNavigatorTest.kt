package fr.jpbarbaud.lydiapp.features.contactdetail

import com.google.common.truth.Truth
import fr.jpbarbaud.lydiapp.libraries.navigationandroid.ContactDetailActivityNavigator
import org.junit.Test

class ContactDetailActivityNavigatorTest {
    @Test
    fun `test ContactDetailActivityNavigator class name destination`() {
        Truth.assertThat(ContactDetailActivityNavigator.CONTACT_DETAIL_ACTIVITY_CLASS_NAME)
            .isEqualTo(ContactDetailActivity::class.java.name)
    }
}
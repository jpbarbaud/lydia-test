plugins {
    plugins("jvm-library")
    kotlin("kapt")
}

dependencies {
    implementation(project(":domain"))
    implementations(
        Dependencies.Hilt.hiltCore,
    )
    kapt("com.google.dagger:hilt-compiler:2.41")
}
package fr.jpbarbaud.lydiapp.presentation.model

data class ContactListPresentationItem(
    val firstName: String,
    val lastName: String,
    val email: String,
    val thumbnail: String
) : PresentationItems()
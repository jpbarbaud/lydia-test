package fr.jpbarbaud.lydiapp.presentation.mapper

import fr.jpbarbaud.lydiapp.domain.model.User
import fr.jpbarbaud.lydiapp.presentation.model.DetailPresentationItem

fun List<User>.toDetailPresentations() = map { it.toDetailPresentation() }

fun User.toDetailPresentation(): DetailPresentationItem = DetailPresentationItem(
    gender = gender.toDetailPresentation(),
    name = name.toDetailPresentation(),
    location = location.toDetailPresentation(),
    email = email,
    login = login.toDetailPresentation(),
    registered = registered,
    dob = dob,
    phone = phone,
    cell = cell,
    id = id.toDetailPresentation(),
    picture = picture.toDetailPresentation(),
    nat = nat.toDetailPresentation()
)

private fun User.Gender.toDetailPresentation(): DetailPresentationItem.Gender = when (this) {
    User.Gender.MALE -> DetailPresentationItem.Gender.MALE
    User.Gender.FEMALE -> DetailPresentationItem.Gender.FEMALE
}

private fun User.Name.toDetailPresentation(): DetailPresentationItem.Name =
    DetailPresentationItem.Name(
        title = title,
        first = first,
        last = last
    )

private fun User.Location.toDetailPresentation(): DetailPresentationItem.Location =
    DetailPresentationItem.Location(
        street = street,
        city = city,
        state = state,
        postcode = postcode
    )

private fun User.Login.toDetailPresentation(): DetailPresentationItem.Login =
    DetailPresentationItem.Login(
        username = username,
        password = password,
        salt = salt,
        md5 = md5,
        sha1 = sha1,
        sha256 = sha256
    )

private fun User.Id.toDetailPresentation(): DetailPresentationItem.Id = DetailPresentationItem.Id(
    name = name,
    value = value
)

private fun User.Picture.toDetailPresentation(): DetailPresentationItem.Picture =
    DetailPresentationItem.Picture(
        large = large,
        medium = medium,
        thumbnail = thumbnail
    )

private fun User.Nat.toDetailPresentation(): DetailPresentationItem.Nat = when (this) {
    User.Nat.AU -> DetailPresentationItem.Nat.AU
    User.Nat.BR -> DetailPresentationItem.Nat.BR
    User.Nat.CA -> DetailPresentationItem.Nat.CA
    User.Nat.CH -> DetailPresentationItem.Nat.CH
    User.Nat.DE -> DetailPresentationItem.Nat.DE
    User.Nat.DK -> DetailPresentationItem.Nat.DK
    User.Nat.ES -> DetailPresentationItem.Nat.ES
    User.Nat.FI -> DetailPresentationItem.Nat.FI
    User.Nat.FR -> DetailPresentationItem.Nat.FR
    User.Nat.GB -> DetailPresentationItem.Nat.GB
    User.Nat.IE -> DetailPresentationItem.Nat.IE
    User.Nat.IR -> DetailPresentationItem.Nat.IR
    User.Nat.NL -> DetailPresentationItem.Nat.NL
    User.Nat.NZ -> DetailPresentationItem.Nat.NZ
    User.Nat.TR -> DetailPresentationItem.Nat.TR
    User.Nat.US -> DetailPresentationItem.Nat.US
}
package fr.jpbarbaud.lydiapp.presentation.features.contactlist

import fr.jpbarbaud.lydiapp.presentation.model.ContactListPresentationItem
import fr.jpbarbaud.lydiapp.presentation.model.DetailPresentationItem
import kotlinx.coroutines.flow.Flow

sealed class ContactListPresentationModelState {
    object Loading : ContactListPresentationModelState()
    data class Success(val contactList: List<ContactListPresentationItem>) :
        ContactListPresentationModelState()

    data class Empty(val message: String) : ContactListPresentationModelState()
    object Error : ContactListPresentationModelState()
    object Done : ContactListPresentationModelState()
}


interface ContactListPresentationModel {
    val detailPresentationResultFlow: Flow<List<DetailPresentationItem>>
    val contactListPresentationResultFlow: Flow<List<ContactListPresentationItem>>
    suspend fun start(): Result<ContactListPresentationModelState>
    suspend fun refresh(): Result<ContactListPresentationModelState>
    suspend fun fetchNext(): Result<ContactListPresentationModelState>

}
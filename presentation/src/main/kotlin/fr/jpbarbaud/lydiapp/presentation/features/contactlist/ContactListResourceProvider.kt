package fr.jpbarbaud.lydiapp.presentation.features.contactlist

interface ContactListResourceProvider {
    val emptyMessage: String
}
package fr.jpbarbaud.lydiapp.presentation.mapper

import fr.jpbarbaud.lydiapp.domain.model.User
import fr.jpbarbaud.lydiapp.presentation.model.ContactListPresentationItem

fun List<User>.toContactListPresentationList(): List<ContactListPresentationItem> =
    this.map { it.toContactListPresentation() }

fun User.toContactListPresentation(): ContactListPresentationItem = ContactListPresentationItem(
    firstName = name.first,
    lastName = name.last,
    email = email,
    thumbnail = picture.thumbnail
)
package fr.jpbarbaud.lydiapp.presentation.features.contactlist

import fr.jpbarbaud.lydiapp.domain.FetchContactsUseCase
import fr.jpbarbaud.lydiapp.domain.model.User
import fr.jpbarbaud.lydiapp.presentation.mapper.toContactListPresentationList
import fr.jpbarbaud.lydiapp.presentation.mapper.toDetailPresentations
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.mapLatest
import javax.inject.Inject
import javax.inject.Named

@OptIn(ExperimentalCoroutinesApi::class)
class ContactListPresentationModelImpl @Inject constructor(
    private val useCase: FetchContactsUseCase,
    private val mapper: ContactListPresentationModelMapper,
    @Named("defaultDispatcher")
    private val dispatcher: CoroutineDispatcher,
) : ContactListPresentationModel {

    private val resultFlow: Flow<List<User>> = useCase.flow()
        .flowOn(dispatcher)

    override val detailPresentationResultFlow = resultFlow.mapLatest {
        it.toDetailPresentations()
    }

    override val contactListPresentationResultFlow = resultFlow.mapLatest {
        it.toContactListPresentationList()
    }

    override suspend fun start(): Result<ContactListPresentationModelState> =
        useCase.fetch().map(mapper::map)

    override suspend fun refresh(): Result<ContactListPresentationModelState> =
        useCase.refresh().map(mapper::map)

    override suspend fun fetchNext(): Result<ContactListPresentationModelState> =
        useCase.fetchNext()
            .map(mapper::map)
}
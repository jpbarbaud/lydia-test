package fr.jpbarbaud.lydiapp.presentation.features.contactlist.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.ContactListPresentationModel
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.ContactListPresentationModelImpl

@Module
@InstallIn(SingletonComponent::class)
abstract class PresentationModule {

    @Binds
    abstract fun provideContactListPresentationModel(
        presentationModel: ContactListPresentationModelImpl
    ): ContactListPresentationModel
}
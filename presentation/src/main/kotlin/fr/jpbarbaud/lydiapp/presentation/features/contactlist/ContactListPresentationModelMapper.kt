package fr.jpbarbaud.lydiapp.presentation.features.contactlist

import fr.jpbarbaud.lydiapp.domain.model.User
import fr.jpbarbaud.lydiapp.presentation.mapper.toContactListPresentationList
import javax.inject.Inject

class ContactListPresentationModelMapper @Inject constructor(
    private val contactListResourceProvider: ContactListResourceProvider
) {
    fun map(list: List<User>): ContactListPresentationModelState =
        if (list.isEmpty()) {
            ContactListPresentationModelState.Empty(contactListResourceProvider.emptyMessage)
        } else {
            ContactListPresentationModelState.Success(list.toContactListPresentationList())
        }
}
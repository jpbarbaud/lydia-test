package fr.jpbarbaud.lydiapp.presentation.mapper

import com.google.common.truth.Truth
import fr.jpbarbaud.lydiapp.presentation.features.contactlist.nextUser
import org.junit.Test
import kotlin.random.Random

class DetailPresentationItemMapper {
    @Test
    fun `Given User When Map to DetailPresentationItem Then result must be valid`() {
        // Given
        val givenUser = Random.nextUser()

        // When
        val expectedModel = givenUser.toDetailPresentation()

        // Then
        with(expectedModel) {
            Truth.assertThat(gender.name).isEqualTo(expectedModel.gender.name)
            Truth.assertThat(name.first).isEqualTo(expectedModel.name.first)

            Truth.assertThat(name.last).isEqualTo(expectedModel.name.last)
            Truth.assertThat(name.title).isEqualTo(expectedModel.name.title)

            Truth.assertThat(location.street).isEqualTo(expectedModel.location.street)
            Truth.assertThat(location.city).isEqualTo(expectedModel.location.city)
            Truth.assertThat(location.state).isEqualTo(expectedModel.location.state)
            Truth.assertThat(location.postcode).isEqualTo(expectedModel.location.postcode)

            Truth.assertThat(email).isEqualTo(expectedModel.email)

            Truth.assertThat(login.username).isEqualTo(expectedModel.login.username)
            Truth.assertThat(login.password).isEqualTo(expectedModel.login.password)
            Truth.assertThat(login.salt).isEqualTo(expectedModel.login.salt)
            Truth.assertThat(login.md5).isEqualTo(expectedModel.login.md5)
            Truth.assertThat(login.sha1).isEqualTo(expectedModel.login.sha1)
            Truth.assertThat(login.sha256).isEqualTo(expectedModel.login.sha256)

            Truth.assertThat(registered).isEqualTo(expectedModel.registered)
            Truth.assertThat(dob).isEqualTo(expectedModel.dob)
            Truth.assertThat(phone).isEqualTo(expectedModel.phone)
            Truth.assertThat(cell).isEqualTo(expectedModel.cell)

            Truth.assertThat(id.name).isEqualTo(expectedModel.id.name)
            Truth.assertThat(id.value).isEqualTo(expectedModel.id.value)

            Truth.assertThat(picture.large).isEqualTo(expectedModel.picture.large)
            Truth.assertThat(picture.medium).isEqualTo(expectedModel.picture.medium)
            Truth.assertThat(picture.thumbnail).isEqualTo(expectedModel.picture.thumbnail)

            Truth.assertThat(nat.name).isEqualTo(expectedModel.nat.name)
        }
    }
}
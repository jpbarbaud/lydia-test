package fr.jpbarbaud.lydiapp.presentation.features.contactlist

import com.google.common.truth.Truth
import fr.jpbarbaud.lydiapp.domain.model.User
import io.mockk.every
import io.mockk.mockk
import org.junit.Test
import kotlin.random.Random


class ContactListPresentationModelMapperTest {

    private val resourceProvider: ContactListResourceProvider = mockk()
    private val mapper = ContactListPresentationModelMapper(resourceProvider)

    @Test
    fun `Given list of users When Map to model state Then result must be success`() {
        // Given
        val userList = List(10) { Random.nextUser() }

        // When
        val state = mapper.map(userList)

        // Then
        Truth.assertThat(state).isInstanceOf(ContactListPresentationModelState.Success::class.java)
        val stateSuccess = state as ContactListPresentationModelState.Success

        userList.forEachIndexed { index, givenUser ->
            val expectedUser = stateSuccess.contactList[index]

            Truth.assertThat(expectedUser.firstName).isEqualTo(givenUser.name.first)
            Truth.assertThat(expectedUser.lastName).isEqualTo(givenUser.name.last)
            Truth.assertThat(expectedUser.email).isEqualTo(givenUser.email)
            Truth.assertThat(expectedUser.thumbnail).isEqualTo(givenUser.picture.thumbnail)
        }
    }

    @Test
    fun `Given empty user list When Map to model state Then result must be empty`() {
        // Given
        val userList = emptyList<User>()
        every { resourceProvider.emptyMessage } returns "This is a test"

        // When
        val state = mapper.map(userList)

        // Then
        Truth.assertThat(state).isInstanceOf(ContactListPresentationModelState.Empty::class.java)
    }
}
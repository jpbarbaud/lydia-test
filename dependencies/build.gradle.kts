plugins {
    `java-platform`
}

dependencies {
    constraints {
        //region Test constraints
        // https://mvnrepository.com/artifact/androidx.test.ext/junit
        api("${Dependencies.Testing.androidXTestJunit}:${Versions.Testing.androidXTestJunit}")

        // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-test
        api("${Dependencies.Testing.coroutinesTest}:${Versions.coroutines}")

        // https://developer.android.com/jetpack/androidx/releases/test
        api("${Dependencies.Testing.espresso}:${Versions.Testing.espresso}")

        // https://dagger.dev/hilt/testing
        api("${Dependencies.Testing.hilt}:${Versions.hilt}")

        // https://github.com/junit-team/junit5
        api("${Dependencies.Testing.junit}:${Versions.Testing.junit}")

        // https://github.com/mockk/mockk
        api("${Dependencies.Testing.mockk}:${Versions.Testing.mockk}")
        api("${Dependencies.Testing.mockkAndroid}:${Versions.Testing.mockk}")

        // https://github.com/google/truth
        api("${Dependencies.Testing.truth}:${Versions.Testing.truth}")

        api("${Dependencies.Testing.okioFakeFileSystem}:${Versions.okio}")
        //endregion

        // Release note : https://developer.android.com/jetpack/androidx/androidx-rn
        // Available packages : https://mvnrepository.com/artifact/androidx
        // Artifact : https://mvnrepository.com/artifact/androidx.annotation/annotation?repo=google
        api("${Dependencies.AndroidX.annotations}:${Versions.AndroidX.annotations}")

        // Artifact : https://mvnrepository.com/artifact/androidx.appcompat/appcompat
        api("${Dependencies.AndroidX.appCompat}:${Versions.AndroidX.appCompat}")

        // Artifact : https://mvnrepository.com/artifact/androidx.constraintlayout/constraintlayout
        // Release notes : https://developer.android.com/jetpack/androidx/releases/constraintlayout
        api("${Dependencies.AndroidX.constraintLayout}:${Versions.AndroidX.constraintLayout}")

        // Artifact : https://mvnrepository.com/artifact/androidx.core/core-ktx
        // Guide : https://developer.android.com/kotlin/ktx#kotlin
        // Release notes : https://developer.android.com/jetpack/androidx/releases/core
        api("${Dependencies.AndroidX.coreKtx}:${Versions.AndroidX.coreKtx}")

        // Artifact : https://mvnrepository.com/artifact/androidx.fragment/fragment-ktx
        // Release notes : https://developer.android.com/jetpack/androidx/releases/fragment
        api("${Dependencies.AndroidX.fragmentKtx}:${Versions.AndroidX.fragment}")

        // Guide : https://developer.android.com/jetpack/androidx/releases/lifecycle
        // Artifact : https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-viewmodel-ktx
        api("${Dependencies.AndroidX.lifecycleViewModelKtx}:${Versions.AndroidX.lifecycle}")

        // Guide : https://developer.android.com/jetpack/androidx/releases/lifecycle
        // Artifact : https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-livedata-ktx
        api("${Dependencies.AndroidX.lifecycleLiveDataKtx}:${Versions.AndroidX.lifecycle}")

        // Guide : https://developer.android.com/guide/topics/ui/layout/recyclerview
        // Artifact : https://mvnrepository.com/artifact/androidx.recyclerview/recyclerview
        api("${Dependencies.AndroidX.recyclerView}:${Versions.AndroidX.recyclerView}")

        // Guide : https://developer.android.com/reference/androidx/recyclerview/selection/package-summary
        // Artifact : https://mvnrepository.com/artifact/androidx.recyclerview/recyclerview-selection
        api("${Dependencies.AndroidX.recyclerViewSelection}:${Versions.AndroidX.recyclerViewSelection}")

        // Guide: https://developer.android.com/jetpack/androidx/releases/swiperefreshlayout
        // Artifact: https://mvnrepository.com/artifact/androidx.swiperefreshlayout/swiperefreshlayout
        api("${Dependencies.AndroidX.swipeRefreshLayout}:${Versions.AndroidX.swipeRefreshLayout}")

        // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-core
        api("${Dependencies.Coroutines.core}:${Versions.coroutines}")

        // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-android
        api("${Dependencies.Coroutines.android}:${Versions.coroutines}")

        // https://dagger.dev/hilt
        api("${Dependencies.Hilt.hiltAndroid}:${Versions.hilt}")
        api("${Dependencies.Hilt.hiltCore}:${Versions.hilt}")
        api("${Dependencies.Hilt.hiltCompiler}:${Versions.hilt}")

        api("${Dependencies.Http.okhttpLoggingInterceptor}:${Versions.okhttp}")
        // Artifact: https://mvnrepository.com/artifact/com.squareup.retrofit2/retrofit
        api("${Dependencies.Http.retrofit}:${Versions.retrofit}")

        // Artifact: https://mvnrepository.com/artifact/com.squareup.retrofit2/converter-moshi
        api("${Dependencies.Http.moshiConverter}:${Versions.retrofit}")

        // Artifact: https://mvnrepository.com/artifact/com.squareup.moshi/moshi
        api("${Dependencies.Http.moshi}:${Versions.moshi}")

        // Artifact: https://mvnrepository.com/artifact/com.squareup.moshi/moshi-kotlin-codegen
        api("${Dependencies.Http.moshiAnnotationProcessor}:${Versions.moshi}")

        // Artifact: https://mvnrepository.com/artifact/com.squareup.okio/okio
        api("${Dependencies.Serialization.json}:${Versions.json}")

        // Artifact: https://mvnrepository.com/artifact/com.github.bumptech.glide/glide
        api("${Dependencies.glide}:${Versions.glide}")

        // Artifact: https://mvnrepository.com/artifact/com.squareup.okio/okio
        api("${Dependencies.okio}:${Versions.okio}")

        // https://material.io/develop/android/docs/getting-started/
        // https://mvnrepository.com/artifact/com.google.android.material/material
        api("${Dependencies.materialDesign}:${Versions.materialDesign}")
    }
}


object Versions {
    object Testing {
        const val androidXTestJunit = "1.1.3"
        const val espresso = "3.4.0"
        const val junit = "4.13.2"
        const val mockk = "1.12.3"
        const val truth = "1.1.3"
    }

    object AndroidX {
        const val annotations = "1.3.0"
        const val appCompat = "1.4.1"
        const val constraintLayout = "2.1.3"
        const val coreKtx = "1.7.0"
        const val fragment = "1.4.1"
        const val lifecycle = "2.4.1"
        const val recyclerView = "1.2.0"
        const val recyclerViewSelection = "1.1.0"
        const val swipeRefreshLayout = "1.1.0"
    }

    const val glide = "4.13.1"
    const val json = "1.3.2"
    const val moshi = "1.13.0"
    const val okio = "3.1.0"
    const val okhttp = "4.9.3"
    const val retrofit = "2.9.0"
    const val hilt = "2.41"
    const val coroutines = "1.6.1"
    const val materialDesign = "1.5.0"
}

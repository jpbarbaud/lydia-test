plugins {
    plugins("jvm-library")
    kotlin("kapt")
}

dependencies {
    implementation(project(":api:remote"))
    implementation(project(":api:local"))
    implementation(project(":libraries:storefile"))
    implementation(project(":domain"))
    implementations(
        Dependencies.Hilt.hiltCore,
        Dependencies.Http.retrofit,
    )
    kapt("com.google.dagger:hilt-compiler:2.41")
}

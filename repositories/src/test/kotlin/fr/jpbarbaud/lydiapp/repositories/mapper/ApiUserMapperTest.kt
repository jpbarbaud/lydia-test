package fr.jpbarbaud.lydiapp.repositories.mapper

import com.google.common.truth.Truth
import fr.jpbarbaud.lydiapp.repositories.random.nextApiUser
import org.junit.Test
import kotlin.random.Random

class ApiUserMapperTest {

    @Test
    fun `Given ApiUser When Map to domain Then result must be valid`() {
        // Given
        val givenApiUser = Random.nextApiUser()

        // When
        val expectedDomain = givenApiUser.toDomain()

        // Then
        with(expectedDomain) {
            Truth.assertThat(gender.name).isEqualTo(givenApiUser.gender.name)
            Truth.assertThat(name.first).isEqualTo(givenApiUser.name.first)

            Truth.assertThat(name.last).isEqualTo(givenApiUser.name.last)
            Truth.assertThat(name.title).isEqualTo(givenApiUser.name.title)

            Truth.assertThat(location.street).isEqualTo(givenApiUser.location.street)
            Truth.assertThat(location.city).isEqualTo(givenApiUser.location.city)
            Truth.assertThat(location.state).isEqualTo(givenApiUser.location.state)
            Truth.assertThat(location.postcode).isEqualTo(givenApiUser.location.postcode)

            Truth.assertThat(email).isEqualTo(givenApiUser.email)

            Truth.assertThat(login.username).isEqualTo(givenApiUser.login.username)
            Truth.assertThat(login.password).isEqualTo(givenApiUser.login.password)
            Truth.assertThat(login.salt).isEqualTo(givenApiUser.login.salt)
            Truth.assertThat(login.md5).isEqualTo(givenApiUser.login.md5)
            Truth.assertThat(login.sha1).isEqualTo(givenApiUser.login.sha1)
            Truth.assertThat(login.sha256).isEqualTo(givenApiUser.login.sha256)

            Truth.assertThat(registered).isEqualTo(givenApiUser.registered)
            Truth.assertThat(dob).isEqualTo(givenApiUser.dob)
            Truth.assertThat(phone).isEqualTo(givenApiUser.phone)
            Truth.assertThat(cell).isEqualTo(givenApiUser.cell)

            Truth.assertThat(id.name).isEqualTo(givenApiUser.id.name)
            Truth.assertThat(id.value).isEqualTo(givenApiUser.id.value)

            Truth.assertThat(picture.large).isEqualTo(givenApiUser.picture.large)
            Truth.assertThat(picture.medium).isEqualTo(givenApiUser.picture.medium)
            Truth.assertThat(picture.thumbnail).isEqualTo(givenApiUser.picture.thumbnail)

            Truth.assertThat(nat.name).isEqualTo(givenApiUser.nat.name)
        }
    }
}
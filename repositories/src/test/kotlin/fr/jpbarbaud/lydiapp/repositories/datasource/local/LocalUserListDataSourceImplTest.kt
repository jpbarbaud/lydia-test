package fr.jpbarbaud.lydiapp.repositories.datasource.local

import com.google.common.truth.Truth
import fr.jpbarbaud.api.local.LocalUserStore
import fr.jpbarbaud.lydiapp.repositories.random.nextLocalUser
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import kotlin.random.Random

@OptIn(ExperimentalCoroutinesApi::class)
class LocalUserListDataSourceImplTest {

    private val localUserStore: LocalUserStore = mockk()

    private val localDataSource = LocalUserListDataSourceImpl(localUserStore)

    @Test
    fun `Given list of random user When fetch list Then value must be valid`() = runTest {
        // Given
        val size = Random.nextInt(1, 10)
        val userList = List(size) { Random.nextLocalUser() }
        coEvery { localUserStore.getAll() } returns userList

        // When
        val list = localDataSource.fetchList()

        // Then
        Truth.assertThat(list).isEqualTo(userList)
    }

    @Test(expected = IllegalStateException::class)
    fun `Given list of random user When fetch list failed Then exception must be thrown`() =
        runTest {
            // Given
            coEvery { localUserStore.getAll() } throws IllegalStateException()

            // When
            localDataSource.fetchList()
        }
}
package fr.jpbarbaud.lydiapp.repositories.mapper

import com.google.common.truth.Truth
import fr.jpbarbaud.api.remote.ApiContactsResponse
import fr.jpbarbaud.lydiapp.repositories.random.nextApiUser
import org.junit.Test
import kotlin.random.Random

class ApiContactResponseMapperTest {

    @Test
    fun `Given ApiContactResponse When Map to domain Then result must be valid`() {
        // Given
        val apiUserList = List(10) { Random.nextApiUser() }

        val givenApiContactResponse = ApiContactsResponse(
            results = apiUserList,
        )

        // When
        val expectedDomain = givenApiContactResponse.toDomain()

        // Then
        Truth.assertThat(expectedDomain.users).isEqualTo(apiUserList.map { it.toDomain() })
    }
}
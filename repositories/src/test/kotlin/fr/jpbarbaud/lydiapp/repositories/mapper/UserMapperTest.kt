package fr.jpbarbaud.lydiapp.repositories.mapper

import com.google.common.truth.Truth
import fr.jpbarbaud.lydiapp.repositories.random.nextUser
import org.junit.Test
import kotlin.random.Random

class UserMapperTest {

    @Test
    fun `Given User When Map to local Then result must be valid`() {
        // Given
        val givenUser = Random.nextUser()

        // When
        val expectedDomain = givenUser.toLocal()

        // Then
        with(expectedDomain) {
            Truth.assertThat(gender.name).isEqualTo(givenUser.gender.name)
            Truth.assertThat(name.first).isEqualTo(givenUser.name.first)

            Truth.assertThat(name.last).isEqualTo(givenUser.name.last)
            Truth.assertThat(name.title).isEqualTo(givenUser.name.title)

            Truth.assertThat(location.street).isEqualTo(givenUser.location.street)
            Truth.assertThat(location.city).isEqualTo(givenUser.location.city)
            Truth.assertThat(location.state).isEqualTo(givenUser.location.state)
            Truth.assertThat(location.postcode).isEqualTo(givenUser.location.postcode)

            Truth.assertThat(email).isEqualTo(givenUser.email)

            Truth.assertThat(login.username).isEqualTo(givenUser.login.username)
            Truth.assertThat(login.password).isEqualTo(givenUser.login.password)
            Truth.assertThat(login.salt).isEqualTo(givenUser.login.salt)
            Truth.assertThat(login.md5).isEqualTo(givenUser.login.md5)
            Truth.assertThat(login.sha1).isEqualTo(givenUser.login.sha1)
            Truth.assertThat(login.sha256).isEqualTo(givenUser.login.sha256)

            Truth.assertThat(registered).isEqualTo(givenUser.registered)
            Truth.assertThat(dob).isEqualTo(givenUser.dob)
            Truth.assertThat(phone).isEqualTo(givenUser.phone)
            Truth.assertThat(cell).isEqualTo(givenUser.cell)

            Truth.assertThat(id.name).isEqualTo(givenUser.id.name)
            Truth.assertThat(id.value).isEqualTo(givenUser.id.value)

            Truth.assertThat(picture.large).isEqualTo(givenUser.picture.large)
            Truth.assertThat(picture.medium).isEqualTo(givenUser.picture.medium)
            Truth.assertThat(picture.thumbnail).isEqualTo(givenUser.picture.thumbnail)

            Truth.assertThat(nat.name).isEqualTo(givenUser.nat.name)
        }
    }
}
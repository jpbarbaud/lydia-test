package fr.jpbarbaud.lydiapp.repositories.datasource.remote

import com.google.common.truth.Truth
import fr.jpbarbaud.api.remote.ApiContactsResponse
import fr.jpbarbaud.api.remote.ContactsApiService
import fr.jpbarbaud.lydiapp.repositories.random.nextApiUser
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import kotlin.random.Random

@OptIn(ExperimentalCoroutinesApi::class)
class RemoteDataSourceImplTest {

    private val api: ContactsApiService = mockk()
    private val remoteDataSource = RemoteDataSourceImpl(api)

    @Test
    fun `Given list of random user When fetch list Then value must be valid`() = runTest {
        // Given
        val size = Random.nextInt(1, 10)
        val expectedResponse = ApiContactsResponse(
            results = List(size) { Random.nextApiUser() },
        )
        coEvery { api.fetchContacts(any(), any()) } returns expectedResponse

        // When
        val givenResponse = remoteDataSource.fetchContacts(1, 10)

        // Then
        Truth.assertThat(givenResponse).isEqualTo(expectedResponse)
    }
}
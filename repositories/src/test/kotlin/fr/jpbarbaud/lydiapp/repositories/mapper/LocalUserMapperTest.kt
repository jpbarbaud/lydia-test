package fr.jpbarbaud.lydiapp.repositories.mapper

import com.google.common.truth.Truth
import fr.jpbarbaud.lydiapp.repositories.random.nextLocalUser
import org.junit.Test
import kotlin.random.Random

class LocalUserMapperTest {

    @Test
    fun `Given LocalUser When Map to domain Then result must be valid`() {
        // Given
        val givenLocalUser = Random.nextLocalUser()

        // When
        val expectedDomain = givenLocalUser.toDomain()

        // Then
        with(expectedDomain) {
            Truth.assertThat(gender.name).isEqualTo(givenLocalUser.gender.name)
            Truth.assertThat(name.first).isEqualTo(givenLocalUser.name.first)

            Truth.assertThat(name.last).isEqualTo(givenLocalUser.name.last)
            Truth.assertThat(name.title).isEqualTo(givenLocalUser.name.title)

            Truth.assertThat(location.street).isEqualTo(givenLocalUser.location.street)
            Truth.assertThat(location.city).isEqualTo(givenLocalUser.location.city)
            Truth.assertThat(location.state).isEqualTo(givenLocalUser.location.state)
            Truth.assertThat(location.postcode).isEqualTo(givenLocalUser.location.postcode)

            Truth.assertThat(email).isEqualTo(givenLocalUser.email)

            Truth.assertThat(login.username).isEqualTo(givenLocalUser.login.username)
            Truth.assertThat(login.password).isEqualTo(givenLocalUser.login.password)
            Truth.assertThat(login.salt).isEqualTo(givenLocalUser.login.salt)
            Truth.assertThat(login.md5).isEqualTo(givenLocalUser.login.md5)
            Truth.assertThat(login.sha1).isEqualTo(givenLocalUser.login.sha1)
            Truth.assertThat(login.sha256).isEqualTo(givenLocalUser.login.sha256)

            Truth.assertThat(registered).isEqualTo(givenLocalUser.registered)
            Truth.assertThat(dob).isEqualTo(givenLocalUser.dob)
            Truth.assertThat(phone).isEqualTo(givenLocalUser.phone)
            Truth.assertThat(cell).isEqualTo(givenLocalUser.cell)

            Truth.assertThat(id.name).isEqualTo(givenLocalUser.id.name)
            Truth.assertThat(id.value).isEqualTo(givenLocalUser.id.value)

            Truth.assertThat(picture.large).isEqualTo(givenLocalUser.picture.large)
            Truth.assertThat(picture.medium).isEqualTo(givenLocalUser.picture.medium)
            Truth.assertThat(picture.thumbnail).isEqualTo(givenLocalUser.picture.thumbnail)

            Truth.assertThat(nat.name).isEqualTo(givenLocalUser.nat.name)
        }
    }
}
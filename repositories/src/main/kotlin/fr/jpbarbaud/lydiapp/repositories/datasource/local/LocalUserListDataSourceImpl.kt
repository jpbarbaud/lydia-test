package fr.jpbarbaud.lydiapp.repositories.datasource.local

import fr.jpbarbaud.api.local.LocalUser
import fr.jpbarbaud.api.local.LocalUserStore
import javax.inject.Inject

class LocalUserListDataSourceImpl @Inject constructor(private val localUserStore: LocalUserStore) :
    LocalUserListDataSource {
    override suspend fun fetchList(): List<LocalUser> = localUserStore.getAll()

    override suspend fun storeList(list: List<LocalUser>) {
        localUserStore.clearAll()
        localUserStore.save(list)
    }

    override suspend fun clear() = localUserStore.clearAll()
}
package fr.jpbarbaud.lydiapp.repositories.datasource.local

import fr.jpbarbaud.api.local.LocalUser

interface LocalUserListDataSource {
    suspend fun fetchList(): List<LocalUser>

    suspend fun storeList(list: List<LocalUser>)

    suspend fun clear()
}
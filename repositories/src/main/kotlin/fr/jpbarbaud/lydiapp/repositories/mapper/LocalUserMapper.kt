package fr.jpbarbaud.lydiapp.repositories.mapper

import fr.jpbarbaud.api.local.LocalUser
import fr.jpbarbaud.lydiapp.domain.model.User

//region domain
internal fun LocalUser.toDomain(): User = User(
    gender = gender.toDomain(),
    name = name.toDomain(),
    location = location.toDomain(),
    email = email,
    login = login.toDomain(),
    registered = registered,
    dob = dob,
    phone = phone,
    cell = cell,
    id = id.toDomain(),
    picture = picture.toDomain(),
    nat = nat.toDomain()
)

private fun LocalUser.Gender.toDomain(): User.Gender = when (this) {
    LocalUser.Gender.MALE -> User.Gender.MALE
    LocalUser.Gender.FEMALE -> User.Gender.FEMALE
}

private fun LocalUser.Name.toDomain(): User.Name = User.Name(
    title = title,
    first = first,
    last = last
)

private fun LocalUser.Location.toDomain(): User.Location = User.Location(
    street = street,
    city = city,
    state = state,
    postcode = postcode
)

private fun LocalUser.Login.toDomain(): User.Login = User.Login(
    username = username,
    password = password,
    salt = salt,
    md5 = md5,
    sha1 = sha1,
    sha256 = sha256
)

private fun LocalUser.Id.toDomain(): User.Id = User.Id(
    name = name,
    value = value
)

private fun LocalUser.Picture.toDomain(): User.Picture = User.Picture(
    large = large,
    medium = medium,
    thumbnail = thumbnail
)

private fun LocalUser.Nat.toDomain(): User.Nat = when (this) {
    LocalUser.Nat.AU -> User.Nat.AU
    LocalUser.Nat.BR -> User.Nat.BR
    LocalUser.Nat.CA -> User.Nat.CA
    LocalUser.Nat.CH -> User.Nat.CH
    LocalUser.Nat.DE -> User.Nat.DE
    LocalUser.Nat.DK -> User.Nat.DK
    LocalUser.Nat.ES -> User.Nat.ES
    LocalUser.Nat.FI -> User.Nat.FI
    LocalUser.Nat.FR -> User.Nat.FR
    LocalUser.Nat.GB -> User.Nat.GB
    LocalUser.Nat.IE -> User.Nat.IE
    LocalUser.Nat.IR -> User.Nat.IR
    LocalUser.Nat.NL -> User.Nat.NL
    LocalUser.Nat.NZ -> User.Nat.NZ
    LocalUser.Nat.TR -> User.Nat.TR
    LocalUser.Nat.US -> User.Nat.US
}
//endregion


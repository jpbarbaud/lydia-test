package fr.jpbarbaud.lydiapp.repositories.mapper

import fr.jpbarbaud.api.remote.ApiUser
import fr.jpbarbaud.api.remote.Gender
import fr.jpbarbaud.api.remote.Id
import fr.jpbarbaud.api.remote.Location
import fr.jpbarbaud.api.remote.Login
import fr.jpbarbaud.api.remote.Name
import fr.jpbarbaud.api.remote.Nat
import fr.jpbarbaud.api.remote.Picture
import fr.jpbarbaud.lydiapp.domain.model.User

internal fun ApiUser.toDomain(): User = User(
    gender = gender.toDomain(),
    name = name.toDomain(),
    location = location.toDomain(),
    email = email,
    login = login.toDomain(),
    registered = registered,
    dob = dob,
    phone = phone,
    cell = cell,
    id = id.toDomain(),
    picture = picture.toDomain(),
    nat = nat.toDomain()
)

private fun Gender.toDomain(): User.Gender = when (this) {
    Gender.MALE -> User.Gender.MALE
    Gender.FEMALE -> User.Gender.FEMALE
}

private fun Name.toDomain(): User.Name = User.Name(
    title = title,
    first = first,
    last = last
)

private fun Location.toDomain(): User.Location = User.Location(
    street = street,
    city = city,
    state = state,
    postcode = postcode
)

private fun Login.toDomain(): User.Login = User.Login(
    username = username,
    password = password,
    salt = salt,
    md5 = md5,
    sha1 = sha1,
    sha256 = sha256
)

private fun Id.toDomain(): User.Id = User.Id(
    name = name,
    value = value
)

private fun Picture.toDomain(): User.Picture = User.Picture(
    large = large,
    medium = medium,
    thumbnail = thumbnail
)

private fun Nat.toDomain(): User.Nat = when (this) {
    Nat.AU -> User.Nat.AU
    Nat.BR -> User.Nat.BR
    Nat.CA -> User.Nat.CA
    Nat.CH -> User.Nat.CH
    Nat.DE -> User.Nat.DE
    Nat.DK -> User.Nat.DK
    Nat.ES -> User.Nat.ES
    Nat.FI -> User.Nat.FI
    Nat.FR -> User.Nat.FR
    Nat.GB -> User.Nat.GB
    Nat.IE -> User.Nat.IE
    Nat.IR -> User.Nat.IR
    Nat.NL -> User.Nat.NL
    Nat.NZ -> User.Nat.NZ
    Nat.TR -> User.Nat.TR
    Nat.US -> User.Nat.US
}
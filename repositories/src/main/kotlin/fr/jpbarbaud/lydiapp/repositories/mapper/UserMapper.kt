package fr.jpbarbaud.lydiapp.repositories.mapper

import fr.jpbarbaud.api.local.LocalUser
import fr.jpbarbaud.lydiapp.domain.model.User

internal fun User.toLocal(): LocalUser = LocalUser(
    gender = gender.toLocal(),
    name = name.toLocal(),
    location = location.toLocal(),
    email = email,
    login = login.toLocal(),
    registered = registered,
    dob = dob,
    phone = phone,
    cell = cell,
    id = id.toLocal(),
    picture = picture.toLocal(),
    nat = nat.toLocal()
)

private fun User.Gender.toLocal(): LocalUser.Gender = when (this) {
    User.Gender.MALE -> LocalUser.Gender.MALE
    User.Gender.FEMALE -> LocalUser.Gender.FEMALE
}

private fun User.Name.toLocal(): LocalUser.Name = LocalUser.Name(
    title = title,
    first = first,
    last = last
)

private fun User.Location.toLocal(): LocalUser.Location = LocalUser.Location(
    street = street,
    city = city,
    state = state,
    postcode = postcode
)

private fun User.Login.toLocal(): LocalUser.Login = LocalUser.Login(
    username = username,
    password = password,
    salt = salt,
    md5 = md5,
    sha1 = sha1,
    sha256 = sha256
)

private fun User.Id.toLocal(): LocalUser.Id = LocalUser.Id(
    name = name,
    value = value
)

private fun User.Picture.toLocal(): LocalUser.Picture = LocalUser.Picture(
    large = large,
    medium = medium,
    thumbnail = thumbnail
)

private fun User.Nat.toLocal(): LocalUser.Nat = when (this) {
    User.Nat.AU -> LocalUser.Nat.AU
    User.Nat.BR -> LocalUser.Nat.BR
    User.Nat.CA -> LocalUser.Nat.CA
    User.Nat.CH -> LocalUser.Nat.CH
    User.Nat.DE -> LocalUser.Nat.DE
    User.Nat.DK -> LocalUser.Nat.DK
    User.Nat.ES -> LocalUser.Nat.ES
    User.Nat.FI -> LocalUser.Nat.FI
    User.Nat.FR -> LocalUser.Nat.FR
    User.Nat.GB -> LocalUser.Nat.GB
    User.Nat.IE -> LocalUser.Nat.IE
    User.Nat.IR -> LocalUser.Nat.IR
    User.Nat.NL -> LocalUser.Nat.NL
    User.Nat.NZ -> LocalUser.Nat.NZ
    User.Nat.TR -> LocalUser.Nat.TR
    User.Nat.US -> LocalUser.Nat.US
}
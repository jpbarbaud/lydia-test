package fr.jpbarbaud.lydiapp.repositories.mapper

import fr.jpbarbaud.api.remote.ApiContactsResponse
import fr.jpbarbaud.lydiapp.domain.model.ContactsResponse

fun ApiContactsResponse.toDomain(): ContactsResponse = ContactsResponse(
    users = results.map { it.toDomain() },
)
package fr.jpbarbaud.lydiapp.repositories.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.jpbarbaud.api.remote.ContactsApiService
import fr.jpbarbaud.lydiapp.domain.repositories.LocalContactListRepository
import fr.jpbarbaud.lydiapp.domain.repositories.RemoteContactListRepository
import fr.jpbarbaud.lydiapp.repositories.LocalContactListRepositoryImpl
import fr.jpbarbaud.lydiapp.repositories.RemoteContactListRepositoryImpl
import fr.jpbarbaud.lydiapp.repositories.datasource.local.LocalUserListDataSource
import fr.jpbarbaud.lydiapp.repositories.datasource.local.LocalUserListDataSourceImpl
import fr.jpbarbaud.lydiapp.repositories.datasource.remote.RemoteDataSource
import fr.jpbarbaud.lydiapp.repositories.datasource.remote.RemoteDataSourceImpl
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoriesModule {

    @Binds
    abstract fun provideRemoteDataSource(datasource: RemoteDataSourceImpl): RemoteDataSource

    @Binds
    abstract fun provideLocalUserListDataSource(datasource: LocalUserListDataSourceImpl): LocalUserListDataSource

    @Binds
    abstract fun provideRemoteContactListRepository(repository: RemoteContactListRepositoryImpl): RemoteContactListRepository

    @Binds
    abstract fun provideLocalContactListRepository(repository: LocalContactListRepositoryImpl): LocalContactListRepository

    companion object {
        @Provides
        fun provideContactsApiService(retrofit: Retrofit): ContactsApiService =
            retrofit.create(ContactsApiService::class.java)
    }
}
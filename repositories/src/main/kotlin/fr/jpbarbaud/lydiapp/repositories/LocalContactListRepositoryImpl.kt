package fr.jpbarbaud.lydiapp.repositories

import fr.jpbarbaud.lydiapp.domain.model.User
import fr.jpbarbaud.lydiapp.domain.repositories.LocalContactListRepository
import fr.jpbarbaud.lydiapp.repositories.datasource.local.LocalUserListDataSource
import fr.jpbarbaud.lydiapp.repositories.mapper.toDomain
import fr.jpbarbaud.lydiapp.repositories.mapper.toLocal
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

class LocalContactListRepositoryImpl @Inject constructor(
    private val datasource: LocalUserListDataSource,
) : LocalContactListRepository {

    private val cacheResults = MutableStateFlow<List<User>>(emptyList())

    override fun flow(): Flow<List<User>> = cacheResults

    override suspend fun fetchList(): List<User> {
        val localList = datasource.fetchList().map { it.toDomain() }
        if (cacheResults != localList) {
            cacheResults.value = localList
        }
        return localList
    }

    override suspend fun storeList(list: List<User>): Result<Unit> {
        val cachedResultList = cacheResults.value

        val actualList = if (cachedResultList.isEmpty().not()) {
            cachedResultList
        } else {
            kotlin.runCatching { fetchList() }.getOrDefault(emptyList())
        }

        val response = actualList + list

        cacheResults.value = response

        datasource.storeList(response.map { it.toLocal() })

        return Result.success(Unit)
    }

    override suspend fun clear() {
        cacheResults.value = emptyList()
        datasource.clear()
    }
}
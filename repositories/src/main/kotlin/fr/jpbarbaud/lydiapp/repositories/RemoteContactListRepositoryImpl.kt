package fr.jpbarbaud.lydiapp.repositories

import fr.jpbarbaud.lydiapp.domain.model.ContactsResponse
import fr.jpbarbaud.lydiapp.domain.repositories.RemoteContactListRepository
import fr.jpbarbaud.lydiapp.repositories.datasource.remote.RemoteDataSource
import fr.jpbarbaud.lydiapp.repositories.mapper.toDomain
import javax.inject.Inject

class RemoteContactListRepositoryImpl @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
) : RemoteContactListRepository {

    override suspend fun fetchContacts(page: Int, resultsPerPage: Int): ContactsResponse =
        remoteDataSource.fetchContacts(page, resultsPerPage).toDomain()
}
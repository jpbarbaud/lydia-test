package fr.jpbarbaud.lydiapp.repositories.datasource.remote

import fr.jpbarbaud.api.remote.ApiContactsResponse
import fr.jpbarbaud.api.remote.ContactsApiService
import javax.inject.Inject

class RemoteDataSourceImpl @Inject constructor(
    private val apiService: ContactsApiService
) : RemoteDataSource {
    override suspend fun fetchContacts(page: Int, resultsPerPage: Int): ApiContactsResponse =
        apiService.fetchContacts(page, resultsPerPage)
}
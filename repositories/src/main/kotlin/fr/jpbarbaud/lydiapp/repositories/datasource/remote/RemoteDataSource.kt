package fr.jpbarbaud.lydiapp.repositories.datasource.remote

import fr.jpbarbaud.api.remote.ApiContactsResponse

interface RemoteDataSource {
    suspend fun fetchContacts(page: Int, resultsPerPage: Int): ApiContactsResponse
}
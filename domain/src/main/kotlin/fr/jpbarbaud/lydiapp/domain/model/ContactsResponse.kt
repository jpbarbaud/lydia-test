package fr.jpbarbaud.lydiapp.domain.model

data class ContactsResponse(
    val users: List<User>
)
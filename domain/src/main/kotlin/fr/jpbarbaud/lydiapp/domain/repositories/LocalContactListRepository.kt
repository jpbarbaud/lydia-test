package fr.jpbarbaud.lydiapp.domain.repositories

import fr.jpbarbaud.lydiapp.domain.model.User
import kotlinx.coroutines.flow.Flow

interface LocalContactListRepository {

    fun flow(): Flow<List<User>>

    suspend fun fetchList(): List<User>

    suspend fun storeList(list: List<User>): Result<Unit>

    suspend fun clear()
}
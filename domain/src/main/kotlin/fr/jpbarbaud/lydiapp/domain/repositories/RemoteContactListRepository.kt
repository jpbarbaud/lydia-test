package fr.jpbarbaud.lydiapp.domain.repositories

import fr.jpbarbaud.lydiapp.domain.model.ContactsResponse

interface RemoteContactListRepository {
    suspend fun fetchContacts(page: Int, resultsPerPage: Int): ContactsResponse
}
package fr.jpbarbaud.lydiapp.domain

import fr.jpbarbaud.lydiapp.domain.model.User
import fr.jpbarbaud.lydiapp.domain.repositories.LocalContactListRepository
import fr.jpbarbaud.lydiapp.domain.repositories.RemoteContactListRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Named
import kotlin.math.floor

class FetchContactsUseCase @Inject constructor(
    private val localContactListRepository: LocalContactListRepository,
    private val remoteRepository: RemoteContactListRepository,
    @Named("defaultDispatcher")
    private val dispatcher: CoroutineDispatcher,
) {

    suspend fun fetch(): Result<List<User>> {
        val existingList = localContactListRepository.fetchList()

        return if (existingList.isNotEmpty()) {
            Result.success(existingList)
        } else {
            fetchRemote(1)
        }
    }

    suspend fun refresh(): Result<List<User>> {
        localContactListRepository.clear()
        return fetch()
    }

    suspend fun fetchNext(): Result<List<User>> {
        val existingList = localContactListRepository.fetchList()

        val currentPage = floor(existingList.size / RESULTS_PER_PAGE.toDouble()).toInt()
        return fetchRemote(currentPage.inc())
    }

    private suspend fun fetchRemote(currentPage: Int): Result<List<User>> =
        kotlin.runCatching {
            remoteRepository.fetchContacts(currentPage, RESULTS_PER_PAGE)
        }.onSuccess { response ->
            localContactListRepository.storeList(response.users)
        }.map { it.users }

    fun flow(): Flow<List<User>> =
        localContactListRepository.flow().flowOn(dispatcher)

    private companion object {
        const val RESULTS_PER_PAGE = 10
    }
}
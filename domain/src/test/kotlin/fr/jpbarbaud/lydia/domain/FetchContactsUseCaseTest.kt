package fr.jpbarbaud.lydia.domain

import com.google.common.truth.Truth
import fr.jpbarbaud.lydiapp.domain.FetchContactsUseCase
import fr.jpbarbaud.lydiapp.domain.model.ContactsResponse
import fr.jpbarbaud.lydiapp.domain.repositories.LocalContactListRepository
import fr.jpbarbaud.lydiapp.domain.repositories.RemoteContactListRepository
import io.mockk.coEvery
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Test
import kotlin.random.Random

@OptIn(ExperimentalCoroutinesApi::class)
class FetchContactsUseCaseTest {

    private val localContactListRepository: LocalContactListRepository = mockk()
    private val remoteRepository: RemoteContactListRepository = mockk()

    private val useCase = FetchContactsUseCase(
        localContactListRepository = localContactListRepository,
        remoteRepository = remoteRepository,
        dispatcher = UnconfinedTestDispatcher()
    )

    @Test
    fun `Given empty list from local repository When fetch is called Then remote repository must be called`() =
        runTest {
            // Given
            val userList = List(10) { Random.nextUser() }
            val response = ContactsResponse(
                users = userList
            )
            coEvery { localContactListRepository.fetchList() } returns emptyList()
            coEvery { remoteRepository.fetchContacts(any(), any()) } returns response
            coJustRun { localContactListRepository.storeList(any()) }

            // When
            val list = useCase.fetch().getOrThrow()

            // Then
            Truth.assertThat(list).isEqualTo(userList)
            coVerify(exactly = 1) {
                localContactListRepository.fetchList()
                remoteRepository.fetchContacts(any(), any())
                localContactListRepository.storeList(userList)
            }
            confirmVerified(localContactListRepository, remoteRepository)
        }

    @Test
    fun `Given list from local repository When fetch is called Then remote repository must not be called`() =
        runTest {
            // Given
            val userList = List(10) { Random.nextUser() }

            coEvery { localContactListRepository.fetchList() } returns userList
            coJustRun { localContactListRepository.storeList(any()) }

            // When
            val list = useCase.fetch().getOrThrow()

            // Then
            Truth.assertThat(list).isEqualTo(userList)
            coVerify(exactly = 1) {
                localContactListRepository.fetchList()
            }
            coVerify(exactly = 0) {
                remoteRepository.fetchContacts(any(), any())
                localContactListRepository.storeList(any())
            }
            confirmVerified(localContactListRepository, remoteRepository)
        }

    @Test
    fun `Given empty list from local repository When fetchNext is called Then page must be equal to 1`() =
        runTest {
            // Given
            val userList = List(10) { Random.nextUser() }
            val response = ContactsResponse(
                users = userList
            )
            val currentPageSlot = slot<Int>()
            coEvery { localContactListRepository.fetchList() } returns emptyList()
            coEvery {
                remoteRepository.fetchContacts(
                    capture(currentPageSlot),
                    any()
                )
            } returns response
            coJustRun { localContactListRepository.storeList(any()) }

            // When
            useCase.fetchNext().getOrThrow()

            // Then
            Truth.assertThat(currentPageSlot.captured).isEqualTo(1)
        }

    @Test
    fun `Given list with n results from local repository When fetchNext is called Then page must be equal to n divided by 10`() =
        runTest {
            // Given
            val expectedPage = Random.nextInt(2, 14)
            val userList = List(expectedPage * 10) { Random.nextUser() }
            val response = ContactsResponse(users = userList)
            val currentPageSlot = slot<Int>()
            coEvery { localContactListRepository.fetchList() } returns userList
            coEvery {
                remoteRepository.fetchContacts(
                    capture(currentPageSlot),
                    any()
                )
            } returns response
            coJustRun { localContactListRepository.storeList(any()) }

            // When
            useCase.fetchNext().getOrThrow()

            // Then
            Truth.assertThat(currentPageSlot.captured).isEqualTo(expectedPage + 1)
        }

    @Test
    fun `When refresh is called Then local repository must be cleared`() = runTest {
        // Given
        val userList = List(10) { Random.nextUser() }
        val response = ContactsResponse(users = userList)
        coJustRun { localContactListRepository.clear() }
        coEvery { localContactListRepository.fetchList() } returns emptyList()
        coEvery { remoteRepository.fetchContacts(any(), any()) } returns response
        coJustRun { localContactListRepository.storeList(any()) }

        // When
        useCase.refresh().getOrThrow()

        // Then
        coVerify {
            localContactListRepository.clear()
            localContactListRepository.fetchList()
            remoteRepository.fetchContacts(1, any())
            localContactListRepository.storeList(userList)
        }
        confirmVerified(localContactListRepository, remoteRepository)
    }
}
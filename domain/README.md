# Domain

This contains the application logic. This is were most of product owner rules will be implemented
and unit tested.

It is standalone, and should rely on almost no other libraries

# Use Case

A UseCase, is globally or a part of user's use case of the app.

It's stateless, all states should be retrieved or stored through repositories.

A UseCase should not throw. If the use case can fail type it and return it. kotlin.Result is a life saver !

plugins {
    plugins("jvm-library")
    kotlin("kapt")
}

dependencies {
    implementations(
        Dependencies.Hilt.hiltCore
    )
    kapt("com.google.dagger:hilt-compiler:2.41")
}
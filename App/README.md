# Application

# Injection

Application (LydiApplication) is the main entry point of the app. 
[Hilt](https://dagger.dev/hilt) is used to provide dependencies in the whole application

package fr.jpbarbaud.lydiapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class LydiApplication : Application()
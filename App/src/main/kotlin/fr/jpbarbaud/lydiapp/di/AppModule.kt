package fr.jpbarbaud.lydiapp.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import java.io.File
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Reusable
    @Named("localUsersFilePathString")
    fun provideLocalUserStore(@ApplicationContext context: Context): String =
        File(context.filesDir.absolutePath, "localUsers.json").absolutePath
}
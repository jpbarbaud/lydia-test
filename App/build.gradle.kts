plugins {
    plugins(
        "android-app",
        "dagger.hilt.android.plugin"
    )
    kotlin("kapt")
}

android {
    defaultConfig {
        applicationId = "fr.jpbarbaud.lydiapp"
        versionCode = 1
        versionName = "0.1.0"
    }
}

dependencies {
    implementation(project(":features:contactList"))
    implementation(project(":features:contactDetail"))
    implementation(Dependencies.Hilt.hiltAndroid)
    kapt("com.google.dagger:hilt-compiler:2.41")
}

kapt {
    correctErrorTypes = true
}
buildscript {
    repositories {
        mavenCentral()
        google()
    }

    dependencies {
        classpath(Dependencies.GradlePlugins.androidBuildToolsGradlePlugin)
        classpath(Dependencies.GradlePlugins.kotlinGradlePlugin)
        classpath(Dependencies.GradlePlugins.hiltGradlePlugin)
    }
}

allprojects {
    repositories {
        mavenCentral()
        google()
    }
}
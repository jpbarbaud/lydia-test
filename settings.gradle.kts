rootProject.name = "Lydiapp"

include(":dependencies")
include(":api:remote", ":api:local")
include(":libraries:commonAndroid", ":libraries:navigationAndroid", ":libraries:storefile")
include(":domain")
include(":repositories")
include(":presentation")
include(":features:contactList", ":features:contactDetail")
include(":App")
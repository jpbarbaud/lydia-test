import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

tasks.withType<KotlinCompile> {
    with(kotlinOptions) {
        jvmTarget = Config.jvmTarget
        freeCompilerArgs = freeCompilerArgs + arrayListOf("-opt-in=kotlin.RequiresOptIn")
    }
}

dependencies {
    implementation(enforcedPlatform(project(":dependencies")))

    implementations(
        Dependencies.Coroutines.core
    )

    testImplementations(
        Dependencies.Testing.coroutinesTest,
        Dependencies.Testing.junit,
        Dependencies.Testing.mockk,
        Dependencies.Testing.truth,
    )
}


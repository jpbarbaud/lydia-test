object Config {
    const val kotlinVersion = "1.6.20"
    const val gradleBuildToolsVersion = "7.1.3"
    const val hiltVersion = "2.41"

    const val jvmTarget = "11"

    object Android {
        const val minSdkVersion = 24
        const val targetSdkVersion = 32
        const val compileSdkVersion = targetSdkVersion
    }
}
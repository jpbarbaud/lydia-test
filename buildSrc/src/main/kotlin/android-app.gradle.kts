plugins {
    id("com.android.application")
    kotlin("android")
}

android {
    compileSdk = Config.Android.compileSdkVersion

    defaultConfig {
        targetSdk = Config.Android.targetSdkVersion
        minSdk = Config.Android.minSdkVersion
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        jvmTarget = Config.jvmTarget
        freeCompilerArgs = freeCompilerArgs + arrayListOf("-opt-in=kotlin.RequiresOptIn")
    }
}

dependencies {
    implementation(enforcedPlatform(project(":dependencies")))

    implementations(
        Dependencies.AndroidX.appCompat,
        Dependencies.AndroidX.constraintLayout,
        Dependencies.AndroidX.coreKtx,
        Dependencies.AndroidX.fragmentKtx,
        Dependencies.AndroidX.lifecycleLiveDataKtx,
        Dependencies.AndroidX.lifecycleViewModelKtx,
        Dependencies.Coroutines.android,
        Dependencies.materialDesign,
    )

    testImplementations(
        Dependencies.Testing.coroutinesTest,
        Dependencies.Testing.junit,
        Dependencies.Testing.mockk,
        Dependencies.Testing.truth,
    )

    androidTestImplementations(
        Dependencies.Testing.androidXTestJunit,
        Dependencies.Testing.espresso,
    )
}
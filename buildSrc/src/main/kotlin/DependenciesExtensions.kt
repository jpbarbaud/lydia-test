import org.gradle.api.artifacts.ProjectDependency
import org.gradle.kotlin.dsl.DependencyHandlerScope
import org.gradle.kotlin.dsl.project
import org.gradle.plugin.use.PluginDependenciesSpec

// https://guides.gradle.org/migrating-build-logic-from-groovy-to-kotlin/#configurations-and-dependencies
// Private methods exist to hide the "dirty" implementation from public methods.

// region Plugins
private fun PluginDependenciesSpec.plugin(plugin: String) = id(plugin)

fun PluginDependenciesSpec.plugins(vararg plugins: String) = plugins.forEach { plugin(it) }
// endregion

// region Implementation
private fun DependencyHandlerScope.implementation(dependency: String)
        = "implementation"(dependency)

private fun DependencyHandlerScope.debugImplementation(dependency: String)
        = "debugImplementation"(dependency)

private fun DependencyHandlerScope.releaseImplementation(dependency: String)
        = "releaseImplementation"(dependency)

private fun DependencyHandlerScope.testImplementation(dependency: String)
        = "testImplementation"(dependency)

private fun DependencyHandlerScope.androidTestImplementation(dependency: String)
        = "androidTestImplementation"(dependency)

fun DependencyHandlerScope.implementations(vararg dependencies: String)
        = dependencies.forEach { implementation(it) }

fun DependencyHandlerScope.debugImplementations(vararg dependencies: String)
        = dependencies.forEach { debugImplementation(it) }

fun DependencyHandlerScope.releaseImplementations(vararg dependencies: String)
        = dependencies.forEach { releaseImplementation(it) }

fun DependencyHandlerScope.testImplementations(vararg dependencies: String)
        = dependencies.forEach { testImplementation(it) }

fun DependencyHandlerScope.androidTestImplementations(vararg dependencies: String)
        = dependencies.forEach { androidTestImplementation(it) }
// endregion

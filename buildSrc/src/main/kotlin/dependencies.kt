object Dependencies {

    object AndroidX {
        // Release note : https://developer.android.com/jetpack/androidx/androidx-rn
        // Available packages : https://mvnrepository.com/artifact/androidx
        // Artifact : https://mvnrepository.com/artifact/androidx.annotation/annotation?repo=google
        const val annotations = "androidx.annotation:annotation"

        // Artifact : https://mvnrepository.com/artifact/androidx.appcompat/appcompat
        const val appCompat = "androidx.appcompat:appcompat"

        const val constraintLayout = "androidx.constraintlayout:constraintlayout"

        // Guide : https://developer.android.com/kotlin/ktx#kotlin
        // Artifact : https://mvnrepository.com/artifact/androidx.core/core-ktx
        const val coreKtx = "androidx.core:core-ktx"

        // Artifact : https://mvnrepository.com/artifact/androidx.fragment/fragment-ktx
        const val fragmentKtx = "androidx.fragment:fragment-ktx"

        // Guide : https://developer.android.com/jetpack/androidx/releases/lifecycle
        // Artifact : https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-viewmodel-ktx
        const val lifecycleViewModelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx"

        // Guide : https://developer.android.com/jetpack/androidx/releases/lifecycle
        // Artifact : https://mvnrepository.com/artifact/androidx.lifecycle/lifecycle-livedata-ktx
        const val lifecycleLiveDataKtx = "androidx.lifecycle:lifecycle-livedata-ktx"

        // Guide : https://developer.android.com/guide/topics/ui/layout/recyclerview
        // Artifact : https://mvnrepository.com/artifact/androidx.recyclerview/recyclerview
        const val recyclerView = "androidx.recyclerview:recyclerview"

        // Guide : https://developer.android.com/reference/androidx/recyclerview/selection/package-summary
        // Artifact : https://mvnrepository.com/artifact/androidx.recyclerview/recyclerview-selection
        const val recyclerViewSelection = "androidx.recyclerview:recyclerview-selection"

        // Guide: https://developer.android.com/jetpack/androidx/releases/swiperefreshlayout
        // Artifact: https://mvnrepository.com/artifact/androidx.swiperefreshlayout/swiperefreshlayout
        const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout"
    }

    object Coroutines {
        // Guide : https://kotlinlang.org/docs/coroutines-guide.html
        // Artifact: https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-core
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core"

        // Guide: https://developer.android.com/kotlin/coroutines
        // Artifact: https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-android
        const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android"
    }

    object GradlePlugins {
        const val androidBuildToolsGradlePlugin = "com.android.tools.build:gradle:${Config.gradleBuildToolsVersion}"
        const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Config.kotlinVersion}"
        const val hiltGradlePlugin = "com.google.dagger:hilt-android-gradle-plugin:${Config.hiltVersion}"
    }

    // Guide: https://developer.android.com/training/dependency-injection/hilt-android
    object Hilt {
        // Artifact: https://mvnrepository.com/artifact/com.google.dagger/hilt-android
        const val hiltAndroid = "com.google.dagger:hilt-android"
        // Artifact: https://mvnrepository.com/artifact/com.google.dagger/hilt-core
        const val hiltCore = "com.google.dagger:hilt-core"
        // Artifact : https://mvnrepository.com/artifact/com.google.dagger/hilt-compiler
        const val hiltCompiler = "com.google.dagger:hilt-compiler"
    }

    object Http {
        // Artifact: https://mvnrepository.com/artifact/com.squareup.okhttp3/logging-interceptor/
        const val okhttpLoggingInterceptor = "com.squareup.okhttp3:logging-interceptor"
        // Artifact: https://mvnrepository.com/artifact/com.squareup.retrofit2/retrofit
        const val retrofit = "com.squareup.retrofit2:retrofit"
        // Artifact: https://mvnrepository.com/artifact/com.squareup.retrofit2/converter-moshi
        const val moshiConverter = "com.squareup.retrofit2:converter-moshi"
        // Artifact: https://mvnrepository.com/artifact/com.squareup.moshi/moshi
        const val moshi = "com.squareup.moshi:moshi"
        // Artifact: https://mvnrepository.com/artifact/com.squareup.moshi/moshi-kotlin-codegen
        const val moshiAnnotationProcessor = "com.squareup.moshi:moshi-kotlin-codegen"
    }

    object Serialization {
        const val json = "org.jetbrains.kotlinx:kotlinx-serialization-json"
    }

    object Testing {
        // https://mvnrepository.com/artifact/androidx.test.ext/junit
        const val androidXTestJunit = "androidx.test.ext:junit"

        // Artifact: https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-test
        const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test"

        // https://developer.android.com/jetpack/androidx/releases/test
        const val espresso = "androidx.test.espresso:espresso-core"

        // Artifact: https://mvnrepository.com/artifact/com.google.dagger/hilt-android-testing
        const val hilt = "com.google.dagger:hilt-android-testing"

        // https://github.com/junit-team/junit5
        const val junit = "junit:junit"

        // https://github.com/mockk/mockk
        const val mockk = "io.mockk:mockk" // for testImplementation
        const val mockkAndroid = "io.mockk:mockk-android" // for androidTestImplementation

        // https://mvnrepository.com/artifact/com.google.truth/truth
        const val truth = "com.google.truth:truth"

        const val okioFakeFileSystem = "com.squareup.okio:okio-fakefilesystem"
    }

    const val glide = "com.github.bumptech.glide:glide"

    // https://mvnrepository.com/artifact/com.squareup.okio/okio
    const val okio = "com.squareup.okio:okio"

    // https://material.io/develop/android/docs/getting-started/
    // https://mvnrepository.com/artifact/com.google.android.material/material
    const val materialDesign = "com.google.android.material:material"
}

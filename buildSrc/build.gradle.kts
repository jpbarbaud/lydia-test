plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    google()
}

dependencies {
    // Android Gradle
    // https://mvnrepository.com/artifact/com.android.tools.build/gradle?repo=google
    implementation("com.android.tools.build:gradle:7.1.3")

    // Kotlin
    // https://github.com/JetBrains/kotlin/blob/master/ChangeLog.md
    // https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-gradle-plugin
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.20")

    // Hilt
    // https://dagger.dev/hilt/gradle-setup.html
    implementation("com.google.dagger:hilt-android-gradle-plugin:2.41")
}
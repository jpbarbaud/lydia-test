# Lydia test

## Versioning

Project is versioned under Git. Let's try as much as possible to follow commit msg formatting
described [here](http://karma-runner.github.io/1.0/dev/git-commit-msg.html).

## Building
Project uses gradle for building and for dependency management. In order to smooth modularization.
It uses precompiled gradle plugins.
[more here ](https://docs.gradle.org/current/userguide/custom_plugins.html#sec:precompiled_plugins).

## Architectures Principles:  Clean architecture and MVVM

Application is split in modules, each one with clearly stated and limited in scope.

Modules that can be pure java, shouldn't be android.

Finally, in order to have a presentation layer agnostic of the view presenting. MVVM has been implemented.

## Modules
[Application](./App/README.md)

[Domain](./domain/README.md)

[Repositories](./repositories/README.md)

[api](./api/README.md)

[libraries](./libraries/README.md)
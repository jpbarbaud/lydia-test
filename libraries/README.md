# Libraries

## commonAndroid

Contain common utils and models used in multiple modules.

## navigationAndroid

This module allows android features to navigate between them without having to know each other.

## storefile

This is an implementation of api module. Pure java module it relies on okio implementation which has
a neat api.

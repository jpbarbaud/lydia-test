package fr.jpbarbaud.libraries.storefile

import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import okio.fakefilesystem.FakeFileSystem
import org.junit.Test
import kotlin.random.Random

@OptIn(ExperimentalCoroutinesApi::class)
class LocalUserStoreImplTest {

    private val fileString = "/test.json"
    private val fileSystem = FakeFileSystem()

    private val localUserStore = LocalUserStoreImpl(
        fileString = fileString,
        fileSystem = fileSystem,
        dispatcher = UnconfinedTestDispatcher()
    )

    @Test
    fun `Given list of user When Save Then cached must be populate`() = runTest {
        // Given
        val size = Random.nextInt(2, 10)
        val userList = List(size) { Random.nextLocalUser() }

        // When
        localUserStore.save(userList)

        // Then
        Truth.assertThat(localUserStore.cached).isEqualTo(userList)
    }

    @Test
    fun `Given empty cache When get list from file Then empty list must be returned`() = runTest {
        // When
        val list = localUserStore.getAll()

        // Then
        Truth.assertThat(list).isEmpty()
    }

    @Test
    fun `Given cache When get list Then cached must be returned`() = runTest {
        // Given
        val size = Random.nextInt(2, 10)
        val userList = List(size) { Random.nextLocalUser() }
        localUserStore.cached = userList

        // When
        val list = localUserStore.getAll()

        // Then
        Truth.assertThat(list).isEqualTo(userList)
    }

    @Test
    fun `Given cache When clear Then cached must be empty`() = runTest {
        // Given
        val size = Random.nextInt(2, 10)
        val userList = List(size) { Random.nextLocalUser() }
        localUserStore.cached = userList

        // When
        localUserStore.clearAll()

        // Then
        Truth.assertThat(localUserStore.cached).isEmpty()
    }
}
package fr.jpbarbaud.libraries.storefile

import androidx.annotation.VisibleForTesting
import fr.jpbarbaud.api.local.LocalUser
import fr.jpbarbaud.api.local.LocalUserStore
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json
import okio.FileNotFoundException
import okio.FileSystem
import okio.Path
import okio.Path.Companion.toPath
import javax.inject.Inject
import javax.inject.Named

class LocalUserStoreImpl @Inject constructor(
    @Named("localUsersFilePathString") fileString: String,
    private val fileSystem: FileSystem,
    @Named("ioDispatcher") private val dispatcher: CoroutineDispatcher
) : LocalUserStore {

    private val filePath: Path = fileString.toPath()

    @get:VisibleForTesting
    internal var cached: List<LocalUser>? = null

    private val serializer = ListSerializer(LocalUser.serializer())

    private val json = Json {
        prettyPrint = true
        isLenient = true
        ignoreUnknownKeys = true
    }

    override suspend fun clearAll() = save(emptyList())

    override suspend fun save(list: List<LocalUser>) {
        runCatching {
            if (cached == list) return@runCatching
            fileSystem.write(filePath) { writeUtf8(json.encodeToString(serializer, list)) }
        }.onSuccess { cached = list }.getOrThrow()
    }

    override suspend fun getAll(): List<LocalUser> = withContext(dispatcher) {
        cached ?: readFromFile()
    }

    private fun readFromFile() = runCatching {
        json.decodeFromString(serializer, fileSystem.read(filePath) { readUtf8() })
    }.onSuccess { cached = it }.recoverCatching {
        if (it !is FileNotFoundException) throw it
        println("$filePath not found, returning empty list")
        return@recoverCatching emptyList()
    }.getOrThrow()
}
package fr.jpbarbaud.libraries.storefile.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import fr.jpbarbaud.api.local.LocalUserStore
import fr.jpbarbaud.libraries.storefile.LocalUserStoreImpl
import okio.FileSystem

@Module
@InstallIn(SingletonComponent::class)
abstract class StoreFileModule {

    @Binds
    abstract fun provideLocalUserStore(impl: LocalUserStoreImpl): LocalUserStore

    companion object {
        @Provides
        fun provideFileSystem(): FileSystem = FileSystem.SYSTEM
    }
}
plugins {
    plugins("jvm-library")
    kotlin("kapt")
}

dependencies {
    implementation(project(":api:local"))

    testImplementations(
        Dependencies.Testing.okioFakeFileSystem
    )

    implementations(
        Dependencies.AndroidX.annotations,
        Dependencies.okio,
        Dependencies.Serialization.json,
        Dependencies.Hilt.hiltCore,
    )
    kapt("com.google.dagger:hilt-compiler:2.41")
}
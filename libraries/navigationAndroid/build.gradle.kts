plugins {
    plugins("android-library")
    kotlin("kapt")
}

dependencies {
    implementation(project(":libraries:commonAndroid"))
    implementations(
        Dependencies.Hilt.hiltCore,
    )
    kapt("com.google.dagger:hilt-compiler:2.41")
}
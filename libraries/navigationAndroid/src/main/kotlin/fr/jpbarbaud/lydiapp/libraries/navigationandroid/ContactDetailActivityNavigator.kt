package fr.jpbarbaud.lydiapp.libraries.navigationandroid

import android.content.Context
import android.content.Intent
import fr.jpbarbaud.lydiapp.libraries.commonandroid.domain.model.ParcelableUser

object ContactDetailActivityNavigator {
    fun newIntent(context: Context, parcelableUser: ParcelableUser): Intent =
        Intent().apply {
            val className = CONTACT_DETAIL_ACTIVITY_CLASS_NAME
            setClassName(context.packageName, className)
            putExtra(CONTACT_DETAIL_ACTIVITY_PRESENTATION_ID_KEY, parcelableUser)
        }

    const val CONTACT_DETAIL_ACTIVITY_PRESENTATION_ID_KEY = "presentationItemKey"
    const val CONTACT_DETAIL_ACTIVITY_CLASS_NAME =
        "fr.jpbarbaud.lydiapp.features.contactdetail.ContactDetailActivity"
}
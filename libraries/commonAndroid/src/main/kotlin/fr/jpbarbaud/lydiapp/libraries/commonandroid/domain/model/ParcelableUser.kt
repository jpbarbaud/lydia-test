package fr.jpbarbaud.lydiapp.libraries.commonandroid.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ParcelableUser(
    val gender: Gender,
    val name: Name,
    val location: Location,
    val email: String,
    val login: Login,
    val registered: Long,
    val dob: Long,
    val phone: String,
    val cell: String,
    val id: Id,
    val picture: Picture,
    val nat: Nat,
) : Parcelable {
    enum class Gender { MALE, FEMALE }

    @Parcelize
    data class Name(
        val title: String,
        val first: String,
        val last: String,
    ) : Parcelable

    @Parcelize
    data class Location(
        val street: String,
        val city: String,
        val state: String,
        val postcode: String,
    ) : Parcelable

    @Parcelize
    data class Login(
        val username: String,
        val password: String,
        val salt: String,
        val md5: String,
        val sha1: String,
        val sha256: String,
    ) : Parcelable

    @Parcelize
    data class Id(
        val name: String,
        val value: String?,
    ) : Parcelable

    @Parcelize
    data class Picture(
        val large: String,
        val medium: String,
        val thumbnail: String,
    ) : Parcelable

    enum class Nat {
        AU, BR, CA, CH, DE, DK, ES, FI, FR, GB, IE, IR, NL, NZ, TR, US
    }
}
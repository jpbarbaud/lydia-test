package fr.jpbarbaud.lydiapp.libraries.commonandroid.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

private const val FADE_IN_DURATION_MS = 200

fun ImageView.load(
    url: String,
    placeholder: Drawable = ColorDrawable(Color.GRAY),
    crossFadeInMs: Duration = FADE_IN_DURATION_MS.milliseconds
) = Glide.with(this.context)
    .load(url)
    .placeholder(placeholder)
    .transition(DrawableTransitionOptions.withCrossFade(crossFadeInMs.inWholeMilliseconds.toInt()))
    .into(this)

fun ImageView.clearLoadings() =
    Glide.with(this).clear(this)
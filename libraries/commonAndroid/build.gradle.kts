plugins {
    plugins(
        "android-library",
        "kotlin-parcelize"
    )
    kotlin("kapt")
}

dependencies {
    implementations(
        Dependencies.glide,
    )
}